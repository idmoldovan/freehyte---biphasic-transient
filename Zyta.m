function [X] = Zyta(Loops, Edges, LHS, RHS, Dim, DimD, DimDa)
% The function Zyta solves the system using a partition
% Original solving system LHS*X=RHS:
%
%    | D     -Ba     -B | | W |   | t |
%    |-Ba'    Da      0 | | Ya| = | 0 |
%    |-B'     0       0 | | Y |   |-u |
%
% where D are the D blocks, Ba are the B blocks that correspond to the
% Robin and Absorbing boundaries, B are the B blocks that correspond to the
% Dirichlet boundaries, Ba' is the transpose complex conjugate of Ba, B'
% is the transpose complex conjugate of B, t are the T blocks, u are the U
% blocks, and X, Ya and Y are the different parts of the solution
% corresponding to each part of LHS and RHS.
%
% D dimension is DimxDim, Ba dimension is DimDxDimDa and B
% dimension is DimDx(Dim-DimD-DimDa)
%
% The partition solver consists of solving first the system
%
%    |-Ba'Za+Da    -Ba'Zy| | Ya| = |  Ba'Zt |
%    |-B'Za        -B'Zy | | Y |   |-u+B'Zt |  
%
% where
%
% Zt = D\t
% Za = D\Ba
% Zy = D\B
%
% After getting Ya and Y from the previous system solution, W can be
% calculated using
%
% W = Zt+ZaYa+ZyY
%
% The entire solution is X = (W,Ya,Y)

%% Initialization
% Initialization of the solution vector X
X = sparse(Dim,1);

% Initialization of the Zy and Za sparse vectors. These vectors only index
% the non-zero values in the Zy and Za matrices
LineZy = zeros(nnz(LHS(1:DimD,DimD+DimDa+1:end)),1);
ColumnZy = zeros(nnz(LHS(1:DimD,DimD+DimDa+1:end)),1);
ValueZy = zeros(nnz(LHS(1:DimD,DimD+DimDa+1:end)),1);
currentZy = 1;

LineZa = zeros(nnz(LHS(1:DimD,DimD+1:DimD+DimDa)),1);
ColumnZa = zeros(nnz(LHS(1:DimD,DimD+1:DimD+DimDa)),1);
ValueZa = zeros(nnz(LHS(1:DimD,DimD+1:DimD+DimDa)),1);
currentZa = 1;

% Initialization of Zt
Zt = zeros(DimD,1);
currentZt = 1;

%% Zy, Za and Zt matrices
% These matrices are build element by element

% Sweeping the elements
for ii=1:length(Loops.order)
    
    % LocElement is a local structure where the features of the current
    % element.
    LocLoop = struct('insert',Loops.insert(ii,:),'dim',...
        Loops.dim(ii,:),'edges',Loops.edges(ii,:));
    
    % Calculating the part of Zt that corresponds to this element (D\t)
    
    zt = LHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1,...
        LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1)\...
        RHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1);  
    
    % inserting zt in the global (all elements) Zt sparse vector
    Zt(currentZt:currentZt+length(zt)-1) = zt;
   
    currentZt = currentZt + length(zt);
    
    % Sweeping the edges of the element to build the Zy and Za parts of 
    % this element
    
    for jj = 1:length(LocLoop.edges)
        
        id = abs(LocLoop.edges(jj));
        
        % LocEdge is a local structure where the features of the current
        % edge.
        
        LocEdge = struct('insert',Edges.insert(id,:),'dim',Edges.dim(id,:),...
            'type',Edges.type(id));
        
        if strcmp(LocEdge.type,'D') % only Dirichlet sides to build Zy
            
            % Calculating the part of Zy that corresponds to this element 
            % (D\B)

            % Defining initial column
            if LocEdge.insert(1) ~= 0
                zyColini = LocEdge.insert(1);
            elseif LocEdge.insert(2) ~= 0
                zyColini = LocEdge.insert(2);
            elseif LocEdge.insert(3) ~= 0
                zyColini = LocEdge.insert(3);
            else
                zyColini = 0;
            end
            
            %Compute only if there are entries in any of the 4 directions
            if zyColini ~= 0 

                % Defining last column
                if LocEdge.insert(3) ~= 0
                    zyColfin = LocEdge.insert(3)+LocEdge.dim(3)-1;
                elseif LocEdge.insert(2) ~= 0
                    zyColfin = LocEdge.insert(2)+LocEdge.dim(2)-1;
                elseif LocEdge.insert(1) ~= 0
                    zyColfin = LocEdge.insert(1)+LocEdge.dim(1)-1;
                end

                zy = LHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1,...
                    LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1)\...
                    (- LHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1,...
                    zyColini:zyColfin));

                % getting the sparse vectors from zy
                [Lzy, Czy, Vzy] = find(zy);

                % inserting in the global (all elements) Zy sparse vector
                LineZy(currentZy:currentZy+length(Lzy)-1) = ...
                    LocLoop.insert(1)+Lzy-1;
                ColumnZy(currentZy:currentZy+length(Czy)-1) = ...
                    LocEdge.insert(1)-DimD-DimDa+Czy-1;
                ValueZy(currentZy:currentZy+length(Vzy)-1) = Vzy;

                currentZy = currentZy + length(Lzy);

            end
            
        elseif strcmp(LocEdge.type,'A') || strcmp(LocEdge.type,'R') 
            % Absorbing and Robin sides to build Za
            
            % Calculating the part of Za that corresponds to this element 
            % (D\Ba)

            % Defining initial column
            if LocEdge.insert(1) ~= 0 % Always if it is an absorbing boundary
                zaColini = LocEdge.insert(1);
            elseif LocEdge.insert(2) ~= 0 
                zaColini = LocEdge.insert(2);
            elseif LocEdge.insert(3) ~= 0 
                zaColini = LocEdge.insert(3);
            else 
                zaColini = 0;
            end
            
            %Compute only if there are entries in any of the 3 directions
            if zaColini ~= 0 

                % Defining last column
                if LocEdge.insert(3) ~= 0 % Always if it is an absorbing boundary
                    zaColfin = LocEdge.insert(3)+LocEdge.dim(3)-1;
                elseif LocEdge.insert(2) ~= 0 
                    zaColfin = LocEdge.insert(2)+LocEdge.dim(2)-1;
                elseif LocEdge.insert(1) ~= 0
                    zaColfin = LocEdge.insert(1)+LocEdge.dim(1)-1;
                end

                za = LHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1,...
                    LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1)\...
                    (- LHS(LocLoop.insert(1):LocLoop.insert(3)+LocLoop.dim(3)-1,...
                    zaColini:zaColfin)); 

                % getting the sparse vectors from za
                [Lza, Cza, Vza] = find(za);

                % inserting in the global (all elements) Za sparse vector
                LineZa(currentZa:currentZa+length(Lza)-1) = ...
                    LocLoop.insert(1)+Lza-1;
                ColumnZa(currentZa:currentZa+length(Cza)-1) = ...
                    LocEdge.insert(1)-DimD+Cza-1;
                ValueZa(currentZa:currentZa+length(Vza)-1) = Vza;

                currentZa = currentZa + length(Lza);

            end
            
        end           
            
    end
    
end

% Build Zy and Za sparse matrices from the sparse vectors. All elements
Zy = sparse(LineZy,ColumnZy,ValueZy,DimD,Dim-DimD-DimDa);
Za = sparse(LineZa,ColumnZa,ValueZa,DimD,DimDa);

%% Solve the first system

% Build the left hand matrix (LHM)
%    |-Ba'Za+Da    -Ba'Zy| 
%    |-B'Za        -B'Zy | 

LHM(1:DimDa,1:DimDa) = LHS(DimD+1:DimD+DimDa,1:DimD)*Za + ...
    LHS(DimD+1:DimD+DimDa,DimD+1:DimD+DimDa);
LHM(DimDa+1:Dim-DimD,1:DimDa) = LHS(DimD+DimDa+1:Dim,1:DimD)*Za;
LHM(1:DimDa,DimDa+1:Dim-DimD) = LHS(DimD+1:DimD+DimDa,1:DimD)*Zy;
LHM(DimDa+1:Dim-DimD,DimDa+1:Dim-DimD) = LHS(DimD+DimDa+1:Dim,1:DimD)*Zy;

% Build the right hand vector (RHV)
%    |  Ba'Zt |
%    |-u+B'Zt |  

RHV(1:DimDa,1) = -LHS(DimD+1:DimD+DimDa,1:DimD)*Zt;
RHV(DimDa+1:Dim-DimD,1) = RHS(DimD+DimDa+1:Dim)-LHS(DimD+DimDa+1:Dim,1:DimD)*Zt;

% Solve the system to get Y and Ya

X(DimD+1:Dim,1) = LHM\RHV; % Ya (from DimD+1 to DimD+DimDa) 
                            % and Y (from DimD+DimDa+1 to Dim)

% Solve the rest of the solution of X (W, from 1 to DimD)

X(1:DimD,1) = Zt+Za*X(DimD+1:DimD+DimDa,1)+Zy*X(DimD+DimDa+1:Dim,1);
    
end
