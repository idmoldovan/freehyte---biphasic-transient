function TimeResults = ComputeTimeResults(SpecResults,T,nel)
% COMPUTETIMERESULTS computes and stores the final displacement and
% stress fields in the dyadic points.
%
% COMPUTETIMERESULTS is called by MAINREG. It receives structure
% SpecResults, where the spectral results are stored, matrix T with the
% wavelet time basis, and the number of finite elements, NEL.
% It returns the cell of structures TIMERESULTS, containing the
% (8) fields Ux, Uy, Wx, ..., Pi computed in a grid of (NoDiv+1)^2
% equally spaced points in every element, and for each dyadic point. This
% means, Ux, Uy, ..., Pi are composed of as many cells as finite elements,
% and in each cell we have a (NoDiv x NoDiv x 2^Ndya+1) matrix, where Ndya
% is specified by the user in GUI1.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developerís 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% The values of the fields stored in TIMERESULTS are computed according to
% expressions (2.50) and (2.70) of reference [2].

%% Sweeping through the finite elements
for ii=1:nel
    
    % Constructing the 4D matrix with the wavelet basis, to fully
    % vectorialize the code
    T4D(1,1,:,:)=T.';
    
    % Multiplying the time basis by the spectral values of the fields.
    % Only the odd-numbered spectral problems are solved, so the fields are
    % multiplied by 2 to account for the results of the even-numbered
    % problems
    TimeResults.Ux{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Ux{ii}, T4D),3)));
    TimeResults.Uy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Uy{ii}, T4D),3)));
    TimeResults.Wx{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Wx{ii}, T4D),3)));
    TimeResults.Wy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Wy{ii}, T4D),3)));
    TimeResults.Sx{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sx{ii}, T4D),3)));
    TimeResults.Sy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sy{ii}, T4D),3)));
    TimeResults.Sxy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sxy{ii}, T4D),3)));
    TimeResults.Pi{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Pi{ii}, T4D),3)));
    
end