function PlotFieldsReg(TimeResults,NoDiv,Nodes,Edges,Loops,Scales,TimeStep,Nx,Ny)
% PLOTFIELDSREG stores and plots the final displacement and stress fields.
%
% PLOTFIELDSREG is called by MAINREG. It receives structures Edges and 
% Loops, the node position list Nodes, the current dyadic point TimeStep,
% and the TimeResults structure, where the results are stored.
% Besides the input arguments, PLOTFIELDSREG loads from STRUCTDEF.mat
% information regarding the file where the results should be stored.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view

%% Managing the result file
% Loading the input data.
% * DirName is the folder to save the result file;
% * FileName is the name of the file where the analysis data was saved;
% * EdgesOrder and LoopsOrder are the orders of the approximation bases of
% the edges and elements, as defined in STRUCTDEF GUI.
load('InputData','DirName','FileName','EdgesOrder','LoopsOrder','PlotStep');

% if the user left DirName blank, does not generate written output
if ~isempty(DirName)  
    
    % Generating the file name
    % removing .mat extension
    FileName = FileName(1:end-4);
    % adding information on the refinement orders to the FileName
    FileName = sprintf('%s_TS%d.dat',FileName,TimeStep);
    % link the path (DirName) to the file name (FileName). This generates
    % the UFilename string, which allows the creation of the result file
    UFilename = fullfile(DirName,FileName);
    % open result file for writing
    FileU = fopen(UFilename,'w');
    
    % Result is used for storing the data in each elelemnt prior to
    % writting the result files
    Result = zeros((NoDiv+1)^2,10);
    
    % The following lines are the header needed to prepare the result file
    % to be readable with TecPlot.
    % Results can also be read by Paraview if the result files are opened
    % with the Tecplot Reader and the Delaunay 2D filter is applied.
    % If you wish to use another software for post-processing, you probably 
    % need to change the header.
    % If the total number of plotting points is odd, one point is not
    % plotted and a warning may pop up if Paraview is used, but the results
    % are plotted anyway.
    % However, the format of the data should be fairly general. The results
    % are written as a matrix of NEL*(NoDiv+1)^2 lines (where NEL is the 
    % total number of finite elements and (NoDiv+1)^2 the total number of 
    % result points per element), and 10 columns. For each result point, the
    % columns list the x and y coordinates, in the global referential, 
    % followed by the values of the displacement and stress fields.
    
    npointsx = Nx*(NoDiv+1);
    npointsy = Ny*(NoDiv+1);


    fprintf(FileU,'TITLE="%s"\n',FileName);
    fprintf(FileU,'VARIABLES="X", "Y", "Ux", "Uy", "Wx", "Wy", "Sx", "Sy", "Sxy", "Pi" \n'); 
    fprintf(FileU,'ZONE T="Time step: %d" , i=%d , j=%d\n',TimeStep,npointsx,npointsy);
    
end

% if the current step number is a multiple of PlotStep, a plot is required
% for this time step 
if (PlotStep && ~rem(TimeStep,PlotStep)) 
    
    % Getting the (global, Cartesian) coordinates of the mesh points
    xmesh = [Nodes(Edges.nini(:),1) Nodes(Edges.nfin(:),1)];
    ymesh = [Nodes(Edges.nini(:),2) Nodes(Edges.nfin(:),2)];
    
    % Scaling back xmesh and ymesh
    xmesh = xmesh * Scales.L;
    ymesh = ymesh * Scales.L;
    
    %% Drawing the mesh, to prepare the plotting of the fields
    % The figure Fig contains 8 plots, three colour maps stress fields, one
    % pore pressure field, two displacement fields and two seepage fields.
    Fig=figure;
    FigName = strcat('TS #',sprintf('%d',TimeStep),...
        ': Real parts of the displacement and stress fields');
    set(Fig,'name',FigName,'numbertitle','off','color','w');
    
    % Preparing the Sx plot
    Sx_Plt = subplot(3,3,1);
    hold on; title('X- Stress');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Sy plot
    Sy_Plt = subplot(3,3,2);
    hold on; title('Y- Stress');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Sxy plot
    Sxy_Plt = subplot(3,3,3);
    hold on; title('XY- Stress');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Pi plot
    Pi_Plt = subplot(3,3,4);
    hold on; title('Pore pressure');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Ux plot
    Ux_Plt = subplot(3,3,5);
    hold on; title('X- Displacement');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Uy plot
    Uy_Plt = subplot(3,3,6);
    hold on; title('Y- Displacement');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Wx plot
    Wx_Plt = subplot(3,3,7);
    hold on; title('X- Seepage');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);
    
    % Preparing the Wy plot
    Wy_Plt = subplot(3,3,8);
    hold on; title('Y- Seepage');
    axis([min(min(xmesh)) max(max(xmesh)) min(min(ymesh)) max(max(ymesh))]);
    % Drawing the mesh, one edge at a time
    for ii=1:length(Edges.type)
        line(xmesh(ii,:),ymesh(ii,:),'color','b');
    end
    daspect([1 1 1]);

end    

%% Initialization of the output points
% Generating the mesh of output points (mapped on a [-1,1] interval).  A
% total of NoDiv+1 points are chosen in each direction, to create NoDiv
% intervals. A small shift is induced to the limits of the interval in
% order to avoid the duplication of the boundary points on adjacent
% elements, which may compromise the visibility of the continuity gaps.
pts = linspace(-(1-1.e-5),(1-1.e-5),NoDiv+1);

% Initializing the maxima & minima of the fields. They are needed
% to scale the colour plots.
Uxmax = -1./eps; Uymax = -1./eps; Wxmax = -1./eps; Wymax = -1./eps;
Sxmax = -1./eps; Symax = -1./eps; Sxymax = -1./eps; Pimax = -1./eps;
Uxmin = 1./eps; Uymin = 1./eps; Wxmin = 1./eps; Wymin = 1./eps;
Sxmin = 1./eps; Symin = 1./eps; Sxymin = 1./eps; Pimin = 1./eps;

%% Building the color maps
% Sweeping the elements to compute the solution fields and draw the colour
% maps
for ii=1:length(Loops.area)
    
    % LocLoop is a local structure where the features of the current
    % element which are directly useful for the calculation of the output
    % fields are stored.
    LocLoop = struct('id',ii,'nodes',Loops.nodes(ii,:),'center',...
        Loops.center(ii,:));
    
    %% Generating the geometric data
    % Computing the length of the sides of the element in x and y direction.
    % Size simply collects the distances between the two most far apart
    % points of the element in x and y directions. ASSUMES THAT THE ELEMENT
    % IS RECTANGULAR!!
    Size = max(Nodes(LocLoop.nodes(:),:))-min(Nodes(LocLoop.nodes(:),:));
    
    % Generating the output grid in local coordinates.
    [x,y] = ndgrid(1/2*Size(1)*pts,1/2*Size(2)*pts);
    
    %% Storing the displacement and stress fields in the result file
    % Computing the global coordinates of the plotting points
    GlobalX = x + LocLoop.center(1);
    GlobalY = y + LocLoop.center(2);
    
    %% Scaling back the dimensions for plotting
    GlobalX = GlobalX * Scales.L;
    GlobalY = GlobalY * Scales.L;
    
    % These copies of the TimeResults fields that correspond to the current
    % dyadic point and finite element are dispensable. However, the code
    % favours legibility, as the computational price is rather low.
    Ux = TimeResults.Ux{ii}(:,:,TimeStep);
    Uy = TimeResults.Uy{ii}(:,:,TimeStep);
    Wx = TimeResults.Wx{ii}(:,:,TimeStep);
    Wy = TimeResults.Wy{ii}(:,:,TimeStep);
    Sx = TimeResults.Sx{ii}(:,:,TimeStep);
    Sy = TimeResults.Sy{ii}(:,:,TimeStep);
    Sxy = TimeResults.Sxy{ii}(:,:,TimeStep);
    Pi = TimeResults.Pi{ii}(:,:,TimeStep);
    
    % Writing the fields in the TecPlot (and Paraview) compatible file,
    % if requested by the user.
    % The results are written as a matrix of NEL*(NoDiv+1)^2 lines
    % (where NEL is the total number of finite elements and (NoDiv+1)^2 the
    % total number of result points per element), and 10 columns. For each
    % result point, the columns list the x and y coordinates, in the global
    % referential, followed by the values of the displacement and stress
    % fields.

    if ~isempty(DirName)
        index=1;
        for jj = 1:NoDiv+1
            for kk = 1:NoDiv+1
                Result(index,1)=GlobalX(jj,kk);
                Result(index,2)=GlobalY(jj,kk);
                Result(index,3)=real(Ux(jj,kk));
                Result(index,4)=real(Uy(jj,kk));
                Result(index,5)=real(Wx(jj,kk));
                Result(index,6)=real(Wy(jj,kk));
                Result(index,7)=real(Sx(jj,kk));
                Result(index,8)=real(Sy(jj,kk));
                Result(index,9)=real(Sxy(jj,kk));
                Result(index,10)=real(Pi(jj,kk));
                index=index+1;
            end
        end
        fprintf(FileU,'%0.6e %0.6e %0.6e %0.6e %0.6e %0.6e %0.6e %0.6e %0.6e %0.6e \n',Result.');
    end
    
    if (PlotStep && ~rem(TimeStep,PlotStep))
        %% Plotting the displacement and stress fields as colour maps
        
        %Sx_Plt;
        colormap jet;
        subplot(3,3,1);
        contourf(GlobalX, GlobalY, real(Sx),20,'edgecolor','none'); 
        axis equal;
        
        %Sy_Plt;
        subplot(3,3,2);
        contourf(GlobalX, GlobalY, real(Sy),20,'edgecolor','none'); 
        axis equal;
        
        %Sxy_Plt;
        subplot(3,3,3);
        contourf(GlobalX, GlobalY, real(Sxy),20,'edgecolor','none'); 
        axis equal;
        
        %Pi_Plt;
        subplot(3,3,4);
        contourf(GlobalX, GlobalY, real(Pi),20,'edgecolor','none'); 
        axis equal;
        
        %Ux_Plt;
        subplot(3,3,5);
        contourf(GlobalX, GlobalY, real(Ux),20,'edgecolor','none'); 
        axis equal;
        
        %Uy_Plt;
        subplot(3,3,6);
        contourf(GlobalX, GlobalY, real(Uy),20,'edgecolor','none'); 
        axis equal;
        
        %Wx_Plt;
        subplot(3,3,7);
        contourf(GlobalX, GlobalY, real(Wx),20,'edgecolor','none'); 
        axis equal;
        
        %Wy_Plt;
        subplot(3,3,8);
        contourf(GlobalX, GlobalY, real(Wy),20,'edgecolor','none'); 
        axis equal;
        
    end
    
    % Updating minima and maxima
    Uxmax = max(Uxmax,max(max(real(Ux))));
    Uymax = max(Uymax,max(max(real(Uy))));
    Wxmax = max(Wxmax,max(max(real(Wx))));
    Wymax = max(Wymax,max(max(real(Wy))));
    Sxmax = max(Sxmax,max(max(real(Sx))));
    Symax = max(Symax,max(max(real(Sy))));
    Sxymax = max(Sxymax,max(max(real(Sxy))));
    Pimax = max(Pimax,max(max(real(Pi))));
    Uxmin = min(Uxmin,min(min(real(Ux))));
    Uymin = min(Uymin,min(min(real(Uy))));
    Wxmin = min(Wxmin,min(min(real(Wx))));
    Wymin = min(Wymin,min(min(real(Wy))));
    Sxmin = min(Sxmin,min(min(real(Sx))));
    Symin = min(Symin,min(min(real(Sy))));
    Sxymin = min(Sxymin,min(min(real(Sxy))));
    Pimin = min(Pimin,min(min(real(Pi))));
    
end

if (PlotStep && ~rem(TimeStep,PlotStep))
    % Setting the colorbar limits to the domain's minima & maxima.
    % overwrite = [-1 0];
    subplot(3,3,1); caxis([Sxmin Sxmax]);  colorbar; %caxis(overwrite);
    subplot(3,3,2); caxis([Symin Symax]); colorbar;
    subplot(3,3,3); caxis([Sxymin Sxymax]); colorbar;
    subplot(3,3,4); caxis([Pimin Pimax]); colorbar;
    subplot(3,3,5); caxis([Uxmin Uxmax]); colorbar;
    subplot(3,3,6); caxis([Uymin Uymax]); colorbar;
    subplot(3,3,7); caxis([Wxmin Wxmax]); colorbar;
    subplot(3,3,8); caxis([Wymin Wymax]); colorbar;
end


%% House cleaning
if ~isempty(DirName)
    fclose('all');
end

end