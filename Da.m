function [Daline, Dacol, Daval, CrtLineDa] = Da(Edges, Loops, Nodes, BConds, Daline, Dacol, Daval, CrtLineDa, NGP)
% Da sweeps through the edges and calls the functions that generate the Da
% blocks of the Absorbing and Robin boundaries in the LHS.
%
% Da is called by MAIN***. It receives as input data the Edges and Loops
% structures, the LHS matrix (that is, the matrix of coefficients of the
% solving system). It returns to MAIN*** the LHS matrix with the Da blocks
% of all elements inserted at the correct positions (as determined in
% ASSIGNPARTS).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB2N2Q4cXZKcGc/view
%
%
% Da computes the internal product (Zai) between the traction basis Z of
% the Absorbing or the Robin boundary and its transpose, multiplied by a
% matrix of constants, specific to each type of boundary. Z is defined by
% Chebyshev polynomials,
%
%        Z  = cos(m*ArcCos(abscissa))
%
% and the internal product is calculated analytically.
%
% For the Absorbing boundary blocks, the matrix multiplier is designed to
% minimize the reflection of the waves from the boundary. Its coefficients
% depend of the wave propagation characteristics, as explained in Section
% 5.3.1 of reference [2]. The enforced absorbing boundary condition is
% given by expression (5.20).
%
% The expression of the Da matrix associated to an absorbing boundary is:
%
%         Da = Delta^(-1)* | C33*Za      0        -C13*Za |
%                          |  0     Delta*C22*Za     0    |
%                          | C13*Za      0         C33*Za |
%     
% where Delta = C11*C33-C13^2, and Cij are defined in reference [2].
%
% For Robin boundary conditions, the Da blocks are calculated multiplying the
% normal and tangential flexibilities (fn and ft) by Zai. The flexibility
% values are given in the GUI. The fluid components (Da31, Da32, Da33) are
% always null.
% When normal or tangential flexibility coefficients are defined as NaN,
% the coresponding block does not exist.
%
% The expression of the Da matrix associated to a Robin boundary is:
%
%       Da = | -fn*Za      0        0 |
%            |   0      -ft*Za      0 |
%            |   0         0        0 |
%
% Further details on the procedure are presented in reference [2].

%% Sweeping the edges
for ii=1:length(Edges.type)
    % Da blocks are constructed for Absorbing and Robin boundaries only.
    if (strcmpi(Edges.type(ii),'A') || strcmpi(Edges.type(ii),'R'))
        % LocEdge is a local structure where the features of the current
        % edge which are directly useful for the calculation of the
        % boundary block are stored.
        LocEdge =struct('parametric',Edges.parametric(ii,:),...
            'lleft',Edges.lleft(ii),'lright',Edges.lright(ii),'order',...
            Edges.order(ii),'insert',Edges.insert(ii,:),...
            'dim',Edges.dim(ii,:),'type',Edges.type(ii));
        % Consistency check: Absorbing and Robin sides cannot have right loops
        if LocEdge.lright
            error('local:consistencyChk',...
                'Exterior edge %d cannot have a right element. \n',...
                LocEdge.id);
        end
        % Generating the Absorbing or Robin boundary block corresponding to 
        % the current edge
        if LocEdge.lleft
            id = LocEdge.lleft;
            % LocLoop is a local structure where the features of the left
            % element which are directly useful for the calculation of the
            % absorbing boundary material properties
            LocLoop = struct('id',id,'materials',Loops.materials(id,:),...
                'vibration',Loops.vibration(id,:));
        else                       % there should always be a left element
            error('local:consistencyChk',...
                'No left loop for edge %d. \n', ii);
        end
        % Launching the function that computes the non-null Absorbing or
        % Robin boundary blocks for the current edge
        [Da11,Da13,Da22,Da33] = Da_Matrix_i(LocEdge,LocLoop,BConds,ii);
        % Inserting the blocks in the global LHS matrix. The 
        % insertion starts at line LocEdge.insert(ii,1) & column 
        % LocEdge.insert(ii,1).
        % For Absorbing boundaries: Da12, Da21 and Da23 are 0 and Da13 =
        % Da31. 
        % For Robin boundaries: Da12, Da13, Da21, Da23, Da31, Da32 and Da33
        % are 0. 
        
        if strcmpi(Edges.type(ii),'A')   % absorbing boundaries
        
            % getting the sparse vectors from Da11
            [LDa11, CDa11, VDa11] = find(Da11);
            
            % Inserting the matrix Da11 in the Da sparse vectors.
            Daline(CrtLineDa:CrtLineDa+length(LDa11)-1) = ...
                LocEdge.insert(1)+LDa11-1;
            Dacol(CrtLineDa:CrtLineDa+length(CDa11)-1) = ...
                LocEdge.insert(1)+CDa11-1;
            Daval(CrtLineDa:CrtLineDa+length(VDa11)-1) = VDa11;

            CrtLineDa = CrtLineDa + length(LDa11);
            
            % getting the sparse vectors from Da13
            [LDa13, CDa13, VDa13] = find(Da13);
            
            % Inserting the matrix Da13 in the Da sparse vectors.
            Daline(CrtLineDa:CrtLineDa+length(LDa13)-1) = ...
                LocEdge.insert(1)+LDa13-1;
            Dacol(CrtLineDa:CrtLineDa+length(CDa13)-1) = ...
                LocEdge.insert(3)+CDa13-1;
            Daval(CrtLineDa:CrtLineDa+length(VDa13)-1) = VDa13;

            CrtLineDa = CrtLineDa + length(LDa13);
            
            Daline(CrtLineDa:CrtLineDa+length(LDa13)-1) = ...
                LocEdge.insert(3)+LDa13-1;
            Dacol(CrtLineDa:CrtLineDa+length(CDa13)-1) = ...
                LocEdge.insert(1)+CDa13-1;
            Daval(CrtLineDa:CrtLineDa+length(VDa13)-1) = VDa13;

            CrtLineDa = CrtLineDa + length(LDa13);
            
            % getting the sparse vectors from Da22
            [LDa22, CDa22, VDa22] = find(Da22);
            
            % Inserting the matrix Da22 in the Da sparse vectors.
            Daline(CrtLineDa:CrtLineDa+length(LDa22)-1) = ...
                LocEdge.insert(2)+LDa22-1;
            Dacol(CrtLineDa:CrtLineDa+length(CDa22)-1) = ...
                LocEdge.insert(2)+CDa22-1;
            Daval(CrtLineDa:CrtLineDa+length(VDa22)-1) = VDa22;

            CrtLineDa = CrtLineDa + length(LDa22);
            
            % getting the sparse vectors from Da33
            [LDa33, CDa33, VDa33] = find(Da33);
            
            % Inserting the matrix Da33 in the Da sparse vectors.
            Daline(CrtLineDa:CrtLineDa+length(LDa33)-1) = ...
                LocEdge.insert(3)+LDa33-1;
            Dacol(CrtLineDa:CrtLineDa+length(CDa33)-1) = ...
                LocEdge.insert(3)+CDa33-1;
            Daval(CrtLineDa:CrtLineDa+length(VDa33)-1) = VDa33;
            
            CrtLineDa = CrtLineDa + length(LDa33);
            
            
        elseif (strcmpi(Edges.type(ii),'R'))  % Robin boundaries
            % if a non-NaN flexibility was defined in the normal direction
            if LocEdge.dim(1)  % it inserts the corresponding boundary block
                
                % getting the sparse vectors from Da11
                [LDa11, CDa11, VDa11] = find(Da11);
                
                % Inserting the matrix Da11 in the Da sparse vectors.
                Daline(CrtLineDa:CrtLineDa+length(LDa11)-1) = ...
                    LocEdge.insert(1)+LDa11-1;
                Dacol(CrtLineDa:CrtLineDa+length(CDa11)-1) = ...
                    LocEdge.insert(1)+CDa11-1;
                Daval(CrtLineDa:CrtLineDa+length(VDa11)-1) = VDa11;
                
                CrtLineDa = CrtLineDa + length(LDa11);
                
            end
            % if a non-NaN flexibility was defined in the tangential direction
            if LocEdge.dim(2)  % it inserts the corresponding boundary block
                
                % getting the sparse vectors from Da22
                [LDa22, CDa22, VDa22] = find(Da22);
                
                % Inserting the matrix Da22 in the Da sparse vectors.
                Daline(CrtLineDa:CrtLineDa+length(LDa22)-1) = ...
                    LocEdge.insert(2)+LDa22-1;
                Dacol(CrtLineDa:CrtLineDa+length(CDa22)-1) = ...
                    LocEdge.insert(2)+CDa22-1;
                Daval(CrtLineDa:CrtLineDa+length(VDa22)-1) = VDa22;
                
                CrtLineDa = CrtLineDa + length(LDa22);
                
            end
        end
    end
end
end

function [Da11, Da13, Da22, Da33] = Da_Matrix_i(LocEdge,LocLoop,BConds,ii)
% Da_MATRIX_i local function computes the Da blocks for the LocEdge edge.
% The side is mapped to a [-1,1] interval to perform the integration.
%% Generating the geometric data
% n + 1 is the current line; 
% m + 1 is the current column.
n = 0:LocEdge.order;
m = 0:LocEdge.order;

[N,M] = ndgrid(n,m);

% Computing the length of the current edge
L = sqrt(LocEdge.parametric(3)^2 + LocEdge.parametric(4)^2); % length


%% Computing the boundary integral
% Analytic expression (valid for M~=N)
Zai = (L/2 * (1./(1-(M+N).^2) + 1./(1-(M-N).^2))) .* (rem(M + N, 2)==0);
% Setting the NaN entries to zero (for M = N+1, the expression above
% yielded NaN)
Zai(isnan(Zai)) = 0;

%% Computing the Da matrix
% For Absorbing boundaries, Da blocks are calculated multiplying the
% inverse of C matrix by the boundary integral matrix Zai.
if strcmpi(LocEdge.type,'A')
        
    % Loading material properties
    alpha = LocLoop.materials(3);
    MM = LocLoop.materials(4);
    LM = LocLoop.materials(11);
    Chi = LocLoop.materials(14);

    % Loading vibration properties
    gamma1 = LocLoop.vibration(1);
    gamma2 = LocLoop.vibration(2);
    beta1 = LocLoop.vibration(4);
    beta2 = LocLoop.vibration(5);
    beta3 = LocLoop.vibration(6);

    % Defining the kind of Hankel function (1 or 2). When the imaginary
    % part of beta is <0, then h=2; when the imaginary part of beta is >0,
    % then h=1. For details, see Section 5.2 of reference [2].
    if imag(beta1) < 0
        h = 2;
    else
        h = 1;
    end

    % Calculating the components of the C matrix that corresponds to the 
    % far-field propagation
    C = (-1.)^(h+1)*1i*MM/(gamma1-gamma2);
    C11 = C*(Chi*(gamma1*beta2-gamma2*beta1)+alpha*gamma1*gamma2*(beta2-beta1));
    C13 = C*(Chi*(beta1-beta2)+alpha*(gamma1*beta1-gamma2*beta2));
    C33 = C*((gamma1*beta1-gamma2*beta2)+alpha*(beta1-beta2));
    C22 = (-1)^(h+1)*1i*beta3*LM;
    Delta = C11*C33-C13^2;
    
    % Calculating the non-null Da components 
    
    Da11 = (C33 / Delta) * Zai;
    Da13 = - (C13 / Delta) * Zai;
    Da22 = C22^(-1) * Zai;
    Da33 = (C11 / Delta)  * Zai;

% For Robin boundaries, Da blocks are calculated multiplying the
% flexibility coefficients by the boundary integral matrix Zai.
elseif strcmpi(LocEdge.type,'R')
         
    Da11 = - BConds.Robin{ii,1} * Zai;
    Da13 = 0;
    Da22 = - BConds.Robin{ii,2} * Zai; 
    Da33 = 0;
    
end
end