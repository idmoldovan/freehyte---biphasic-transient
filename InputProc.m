function [NGP, Nodes, Edges, Loops, BConds, ohm, Scales] = ...
    InputProc(ohm,spectral)
% INPUTPROC is the input processing function for regular and non-regular
% meshes. 
%
% INPUTPROC is called by MAIN***. It reads the input data inserted in 
% the GUIs and organizes it in the data structures Edges, Loops and BConds.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB2N2Q4cXZKcGc/view
%
% INPUT DATA passed by MAIN***
% * ohm - spectral frequency (complex)
% * spectral - matrix of as many lines as Dirichlet and Neumann boundaries
% and 2 columns. In the first column, the index of the current boundary is
% stored. In the second column, we have the spectral components of the
% applied boundary conditions on each of these boundaries, corresponding to
% the current frequency.
%
% INPUT DATA read from the *.mat files created by the GUIs:
% * from STRUCTDEF: Young (Young's modulus), Poisson (Poisson coefficient),
% alpha (Biot's coefficient), M (Biot's modulus), rho (density of the
% mixture), rhow (density of the fluid), a (tortuosity), nw (volume
% fraction), k (hydraulic conductivity), 
% NumberGaussPoints (the nuber of Gauss-Legendre points for the side
% integration), EdgesOrder and LoopsOrder (the orders of the approximation
% bases on the essential edges of the mesh and in the finite elements);
% * from STRUCTREGBC1: nodes, edges_nodes, edges_loops, loops_nodes and 
% loops_edges. These variables contain geometrical and topological 
% information regarding the mesh. For a full description of these
% variables, please consult [4];
% * from STRUCTREGBC2: edgesDirichlet, edgesNeumann, edgesRobin, 
% edgesAbsorbing, dataDir, dataNeu, dataRobin; 
% * edgesDirichlet (edgesNeumann, edgesRobin, edgesAbsorbing): list with 
% the Dirichlet (Neumann, Robin, Absorbing) edges; 
% * dataDir (dataNeu, dataRobin, dataAbsorbing): cell array with five
% columns (Dirichlet or Neumann), or three columns (Robin),
% or one column (Absorbing) and as many lines as Dirichlet (Neumann, Robin,
% Absorbing) edges. For each edge, it stores the edge's index. For Neumann
% and Dirichlet boundaries, it stores the boundary conditions in the normal
% and tangential directions in the solid phase and in the fluid phase.
% For Robin boundaries, it stores the flexibility coefficients in the
% normal and tangential directions.
% The boundary conditions are stored as strings.
% For further details regarding the definition of the boundary conditions,
% please consult reference [3].
%
%
% OUTPUT DATA (to function MAIN***):
% * NGP is the number of Gauss points for the line integration;
% * Nodes is a (NNODE x 2) matrix, where NNODE is the number of nodes in 
% mesh. It stores the coordinates of each node;
% * Edges, Loops and BConds are data structures storing information
% on the edges, finite elements (loops) and boundary conditions,
% respectively. They are documented in reference [3];
% * ohm is the spectral frequency, scaled by Scales.Ohm
% * Scales is a structure containing the scaling factors for the physical
% (material, dimensions, loads) scaling

%% Loading data
load('InputData','Young','Poisson','alpha','M','rho','rhow','a','nw',...
    'k','g','TimeOrder','NumberGaussPoints','EdgesOrder','LoopsOrder',...
    'edgesDirichlet','edgesNeumann','dataDir','dataNeu',...
    'edgesRobin','dataRobin','edgesAbsorbing','nodes','edges_nodes','edges_loops',...
    'loops_nodes','loops_edges');

%% Initialization of the scaling coefficients
% L - scaling for all dimensions and coordinates, equal to the maximum
% length of a side
% E - scaling for all moduli, equal to the Young's modulus input by the
% user in the GUI
% Rho - scaling for all densities, equal to the mixture density input by
% the user in the GUI
% F - scaling for tractions and stresses, equal to the maximum value of all
% tractions
% U - scaling for the displacements, equal to F*L/E
% Ohm - scaling for the frequency, equal to 1/L*sqrt(E/Rho)
% Xi - scaling for the dissipation, equal to sqrt(E*Rho)/L
% Flex - scaling for the flexibility coefficients on the Robin and
% absorbing boundaries, equal to L/E
Scales = struct('L',1,'E',Young,'Rho',rho,...
    'F',1,'U',1,'Ohm',1,'Xi',1,'Flex',1); 

%% MESH DATA
% Creation of the Nodes, Edges and Loops data structures
% Definition of the mesh-related data Nodes, and data structures Edges and
% Loops. For a full description of these data structure, please refer to
% reference [4].
Nodes = nodes;
Edges=struct('nini',edges_nodes(:,1),'nfin',edges_nodes(:,2),...
    'parametric',createLine(Nodes(edges_nodes(:,1),:),...
    Nodes(edges_nodes(:,2),:)),...
    'lleft',edges_loops(:,1),'lright',edges_loops(:,2),...
    'type',char(zeros(length(edges_nodes(:,1)),1)),'order',...
    zeros(length(edges_nodes(:,1)),1)); 
Edges.type(:) = 'D';           % all edges are predefined as Dirichlet 
Edges.order(:) = NaN;          % all degrees are predefined as NaN
Loops=struct('nodes',loops_nodes,'edges',loops_edges,...
    'center',zeros(length(loops_nodes(:,1)),2),...
    'area',zeros(length(loops_nodes(:,1)),1),...
    'order',zeros(length(loops_nodes(:,1)),1),...
    'materials',zeros(length(loops_nodes(:,1)),19),...
    'vibration',zeros(length(loops_nodes(:,1)),9));

% Computation of the barycenter and area of each finite element. It uses
% the POLYGONCENTROID function by David Legland.
for i=1:length(Loops.area)
    [Loops.center(i,:),Loops.area(i)] = ...
        polygonCentroid(Nodes(loops_nodes(i,:),:));
    Loops.area(i)=abs(Loops.area(i)); 
end

% Scaling factors for lengths, frequency and dissipation
Scales.L = max(sqrt(Edges.parametric(:,3).^2+Edges.parametric(:,4).^2));
Scales.Ohm = (1/Scales.L)*sqrt(Scales.E/Scales.Rho);
Scales.Xi = sqrt(Scales.E*Scales.Rho)/Scales.L;
% Performing the scaling for lengths
Nodes = Nodes/Scales.L;
Edges.parametric = Edges.parametric/Scales.L;
Loops.area = Loops.area/(Scales.L)^2;
Loops.center = Loops.center/Scales.L;
% Performing scaling for frequency
ohm = ohm/Scales.Ohm;


%% RUN CONTROL DATA
% Loading algorithmic, refinement and edge data
NGP = NumberGaussPoints;   %Number of Gauss integration points per interval

%% EDGE TYPE DATA
% Registration of the Neumann, Absorbing and Robin edges, using
% edgesNeumann, edgesAbsorbing and edgesRobin vectors from the GUI.
% It is recalled that all edges were predefined as Dirichlet. 
% Users may overwrite the data loaded from the GUI to change the boundary
% types. An example is given below.
if exist('edgesNeumann')
    for i=1:length(edgesNeumann)
        Edges.type(edgesNeumann(i),1) = 'N'; 
    end
end
% Manual definition of Neumann edges, if needed
%Edges.type(17:20) = 'N';       <------- Examples       
%Edges.type(25:5:40) = 'N';     <------- Examples

if exist('edgesAbsorbing')
    for i=1:length(edgesAbsorbing)
        Edges.type(edgesAbsorbing(i),1) = 'A'; 
    end
end
% Manual definition of Absorbing edges, if needed
%Edges.type(17:20) = 'A';       <------- Examples       
%Edges.type(25:5:40) = 'A';     <------- Examples

if exist('edgesRobin')
    for i=1:length(edgesRobin)
        Edges.type(edgesRobin(i),1) = 'R'; 
    end
end
% Manual definition of Robin edges, if needed
%Edges.type(17:20) = 'R';       <------- Examples       
%Edges.type(25:5:40) = 'R';     <------- Examples

%% EDGE REFINEMENT DATA
% Allocates the refinement order defined in the GUI to all Dirichlet,
% Absorbing and Robin boundaries
Edges.order(Edges.type=='D') = EdgesOrder;
Edges.order(Edges.type=='A') = EdgesOrder;
Edges.order(Edges.type=='R') = EdgesOrder;
% ... or overwrite orders manually, if you need to have different orders
% Edges.order(13) = 3;       <------- Examples
% Edges.order(17) = 5;       <------- Examples

%% ELEMENT DATA
% Allocates the refinement order defined in the GUI to all elements
Loops.order(:) = LoopsOrder;
% ... or overwrite orders manually, if you need to have different orders
% for different elements
% Loops.order(1) = 5;       <------- Examples
% Loops.order(2) = 5;       <------- Examples
% Loops.order(3) = 5;       <------- Examples

%% MATERIAL DATA
% Computes the material and vibration characteristics for each elements and
% stores them in the Loop structure. Due to the complex nature of this
% process, it is handled by the external function COMPUTEMATERIALPROP. 
% The material data is assumed uniform for all elements. However, users may
% overwrite the data loaded from the GUI to define elements with distinct
% material properties. Examples are given below.

% Allocate the material characteristics defined in the GUI to all elements
Loops.materials(:,1) = Young;
Loops.materials(:,2) = Poisson;
Loops.materials(:,3) = alpha;
Loops.materials(:,4) = M;
Loops.materials(:,5) = rho;
Loops.materials(:,6) = rhow;
Loops.materials(:,7) = a;
Loops.materials(:,8) = nw;
Loops.materials(:,9) = k;
% ... or overwrite the material characteristics manually. However, please
% be aware that the scales for the material data are related to the data
% input in the GUI.
% Loops.materials(5,1) = 1000;      <------- Examples
% Loops.materials(5,2) = 0.2;      <------- Examples

% Calling the function that computes the material and vibration properties
% and fills the Loops.materials and Loops.vibration fields. All material
% properties coming out are scaled. 
Loops = ComputeMaterialProp(Loops,Scales,ohm,g);


%% LOAD, DISPLACEMENT & FLEXIBILITY DATA 
% Dirichlet and Neumann boundary conditions can be described by polynomials
% of any order. The definition of Dirichlet and Neumann boundary condition
% is made by specifying its values in as many equally spaced points along
% the boundary as needed to define its polynomial variation.
% The definition of the Robin boundary conditions is made by specifying the
% normal and tangential flexibility coefficients.
% For further details regarding the definition of the boundary conditions,
% please refer to reference [3].
%
% BConds data structure collects information regarding the boundary 
% conditions. Its members are cell arrays with as many lines as the 
% external boundaries of the structure. The values of the forces (or
% displacements) enforced on the boundaries are stored in the Neumann (or
% Dirichlet) fields of the structure. The values of the normal and
% tangential flexibility coefficients are stored in the Robin fields.
% When a Robin boundary does not have a normal or tangential flexibility
% coefficient defined, the value is stored as NaN.
% NaN is stored in the Dirichlet field of a Neumann or Robin boundary,
% NaN is stored in the Neumann field of a Dirichlet or Robin boundary,
% and NaN is stored in the Robin field of a Dirichlet or Neumann boundary.

% Initialization of the BConds structure
BConds=struct('Neumann',{cell(length(edges_nodes),3)},'Dirichlet',...
    {cell(length(edges_nodes),3)},'Robin',{cell(length(edges_nodes),2)});
BConds.Neumann(:) = {NaN};
BConds.Dirichlet(:) = {NaN};
BConds.Robin(:) = {NaN};

% Neumann boundary conditions are defined in the boundary normal and
% tangential directions in the solid phase and in the normal direction in
% the fluid phase, and assumed to be of the form,
% F(x,t) = f(x) * T(time)
% f(x) is a polynomial function, defined here through its values in an
% arbitrary number of equally spaced points on the boundary. T(time) is an
% arbitrary function of time, defined by any valid Matlab command. 

% Neumann boundary conditions are imported from the GUI and stored in the
% Neumann field of the structure. MULTIPLIER is the spectral component of
% the time variation T(time). Users may overwrite the data loaded from the
% GUI to change the boundary conditions. An example is given below. 

if exist('dataNeu')
    % Getting the scale for the applied tractions
    Scales.F = 0;
    for ii=1:size(dataNeu,1)
        % getting the current edge
        crtedge = dataNeu{ii,1};
        % extracting the spectral multiplier. It is found by looking for
        % crtedge in the first column of the 'spectral' matrix and picking
        % the value in the corresponding second column.
        multiplier = spectral(spectral(:,1)==crtedge,2);
        % getting the list of loads, in all phases, on the current edge
        LoadValues = cellfun(@str2num,dataNeu(ii,2:4),'UniformOutput',0);
        % flattening, multiplying by the multiplier, and taking the
        % absolute value
        LoadValues = abs(horzcat(LoadValues{:})*multiplier);
        if max(LoadValues > Scales.F)
            Scales.F = max(LoadValues);
        end
    end
    % if all forces are zero, it sets the scale to 1
    if abs(Scales.F) < eps
        Scales.F = 1;
    end
    for ii=1:size(dataNeu,1)
        % getting the current edge
        crtedge = dataNeu{ii,1};
        % extracting the spectral multiplier. It is found by looking for
        % crtedge in the first column of the 'spectral' matrix and picking
        % the value in the corresponding second column.
        multiplier = spectral(spectral(:,1)==crtedge,2);
        % getting the scaled traction values
        BConds.Neumann{crtedge,1} = ...
            str2num(dataNeu{ii,2})*multiplier;
        BConds.Neumann{crtedge,2} = ...
            str2num(dataNeu{ii,3})*multiplier;
        BConds.Neumann{crtedge,3} = ...
            str2num(dataNeu{ii,4})*multiplier;
    end
    % overwrite boundary conditions manually
    % BConds.Neumann(17,1) = {[0 0.25]}*multiplier;  <------- Examples
    % BConds.Neumann(17,2) = {[0 0.25]}*multiplier;  <------- Examples
    
    % Scaling the Neumann boundary conditions
    BConds.Neumann = cellfun(@(x) x/Scales.F,BConds.Neumann,'UniformOutput',0);
end

% Dirichlet boundary conditions are defined in the boundary normal and
% tangential directions in the solid phase and in the normal direction in
% the fluid phase, and assumed to be of the form,
% U(x,t) = u(x) * T(time)
% u(x) is a polynomial function, defined here through its values in an
% arbitrary number of equally spaced points on the boundary. T(time) is an
% arbitrary function of time, defined by any valid Matlab command. 

% Dirichlet boundary conditions are imported from the GUI and stored in the
% Dirichlet field of the structure. MULTIPLIER is the spectral component of
% the time variation T(time). Users may overwrite the data loaded from the
% GUI to change the boundary conditions. An example is given below.  
if exist('dataDir')
    for ii=1:size(dataDir,1)
        % getting the current edge
        crtedge = dataDir{ii,1};
        % extracting the spectral multiplier. It is found by looking for
        % crtedge in the first column of the 'spectral' matrix and picking
        % the value in the corresponding second column.
        multiplier = spectral(spectral(:,1)==crtedge,2);
        BConds.Dirichlet{crtedge,1} = ...
            str2num(dataDir{ii,2})*multiplier;
        BConds.Dirichlet{crtedge,2} = ...
            str2num(dataDir{ii,3})*multiplier;
        BConds.Dirichlet{crtedge,3} = ...
            str2num(dataDir{ii,4})*multiplier;
    end
    % overwrite boundary conditions manually
    % BConds.Dirichlet(17,1) = {[0 0.25]}*multiplier;  <------- Examples
    % BConds.Dirichlet(17,2) = {[0 0.25]}*multiplier;  <------- Examples
    
    % Scaling the Dirichlet boundary conditions
    Scales.U = Scales.F * Scales.L / Scales.E;
    BConds.Dirichlet = cellfun(@(x) x/Scales.U,BConds.Dirichlet,'UniformOutput',0);    
end

% Robin boundary conditions are the flexibility coefficients defined in the
% boundary normal and tangential directions of the solid phase.
% Robin boundary conditions are imported from the GUI and stored in the
% Robin field of the BConds structure. Users may overwrite the data loaded
% from the GUI to change the boundary conditions. An example is given below. 
if exist('dataRobin')
    for ii=1:size(dataRobin,1)
        % getting the current edge
        crtedge = dataRobin{ii,1};
        BConds.Robin{crtedge,1} = str2num(dataRobin{ii,2});
        BConds.Robin{crtedge,2} = str2num(dataRobin{ii,3});
    end
    % overwrite boundary conditions manually
    % BConds.Robin(17,1) = {1e-9};  <------- Examples
    % BConds.Robin(17,2) = {1e-9};  <------- Examples
    
    % Scaling the Robin boundary conditions
    Scales.Flex = Scales.L / Scales.E;
    BConds.Robin = cellfun(@(x) x/Scales.Flex,BConds.Robin,'UniformOutput',0);
end

end
