function Loops = ComputeMaterialProp(Loops,Scales,ohm,g)
% COMPUTEMATERIALPROP computes the material and dynamic properties and
% stores them in the Loops structure (in Loops.materials and
% Loops.vibration).
%
% COMPUTEMATERIALPROP is called by INPUTPROCREG. Receives the Loops
% structure, the spectral frequency ohm and the gravitational acceleration
% g and returns the Loops structure filled with the material and dynamic
% parameters computed in this routine.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphaisc HTD User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
%
% The material properties collected in Loops.materials are as follows:
%
%   No	Material property       Notation	How is it obtained?
%   1	Young's modulus         Young       GUI
%   2	Poisson's coefficient	Poisson     GUI
%   3	Biot's coefficient      alpha       GUI
%   4	Biot's modulus          M           GUI
%   5	Mixture density         rho         GUI
%   6	Fluid density           rhow        GUI
%   7	Tortuosity              a           GUI
%   8	Liquid volume fraction	nw          GUI
%   9	Hydraulic conductivity	k           GUI
%   10	Lam�'s Lambda           LL          (B.5), reference [2] (for all)
%   11	Lam�'s Mu (shear mod.)	LM          (B.3)
%   12	Dissipation             xi          (B.15)
%   13	Complex density/damping	rhow2       (4.4)
%   14	-                       Chi         (4.23)
%   15	k11                     k11         (4.8)
%   16	k12                     k12         (4.8)
%   17	k14                     k14         (4.8)	
%   18	k33                     k33         (4.8)	
%   19	k44                     k44         (4.8)
%
%
% The dynamic properties collected in Loops.vibration are as follows:
%
%   No	Dynamic property        Wave type       Notation        Expression
%   1   Proportionality ct      P1              gamma1          (4.22)
%   2                           P2              gamma2          (4.22)	
%   3                           S               gamma3          (4.29)
%   4	Wave number             P1              beta1           (4.24)
%   5                           P2              beta2           (4.24)
%   6                           S               beta3           (4.32)
%   7	Wavelength              P1              wl1             (4.26)
%   8                           P2              wl2             (4.26)	
%   9                           S               wl3             (4.34)
%
% The expressions refer to reference [2].

% Sweeping through the elements. The for cycle is avoidable here, but it
% may complicate matters when the rearrangement of the wave numbers takes
% place at the end. Since the lost time shouldn't be too great, the for
% cycle is left, for clarity.

%% Sweeping through the elements and computing their material and vibration 
%  characteristics
for ii=1:length(Loops.area)
    
    % To enhance the legibility of the routine, the notations in the above
    % tables are preferred over descriptions like Loops.materials(ii,jj).
    Young = Loops.materials(ii,1);
    Poisson = Loops.materials(ii,2);
    alpha = Loops.materials(ii,3);
    M = Loops.materials(ii,4);
    rho = Loops.materials(ii,5);
    rhow = Loops.materials(ii,6);
    a = Loops.materials(ii,7);
    nw = Loops.materials(ii,8);
    k = Loops.materials(ii,9);
    
    % Computing the remaining material properties
    LL = Poisson*Young/(1+Poisson)/(1-2*Poisson); 
    LM = Young/2/(1+Poisson);               
    xi = rhow*g/k*(nw)^2;                   
    
    % Scaling the material properties and overwriting their definitions in
    % Loops
    Young = Young/Scales.E;         Loops.materials(ii,1) = Young;
    M = M/Scales.E;                 Loops.materials(ii,4) = M;
    rho = rho/Scales.Rho;           Loops.materials(ii,5) = rho;
    rhow = rhow/Scales.Rho;         Loops.materials(ii,6) = rhow;
    LL = LL/Scales.E;               Loops.materials(ii,10)=LL;
    LM = LM/Scales.E;               Loops.materials(ii,11) = LM;
    xi = xi/Scales.Xi;              Loops.materials(ii,12) = xi;
    
    rhow2 = rhow*a/nw - 1i*xi/ohm/(nw)^2;   Loops.materials(ii,13)=rhow2;
    Chi = alpha^2 + (LL+2*LM)/M;            Loops.materials(ii,14)=Chi;
    k12 = LL + alpha^2*M;                   Loops.materials(ii,16)=k12;
    k33 = LM;                               Loops.materials(ii,18)=k33;
    k11 = k12 + 2*k33;                      Loops.materials(ii,15)=k11;
    k14 = alpha*M;                          Loops.materials(ii,17)=k14;
    k44 = M;                                Loops.materials(ii,19)=k44;
    
    % Computing the dynamic properties
    % Setting up the quadratic equation to find the proportionality
    % constants of the compressional wave
    Eqn = [1-alpha*rhow2/rhow...
           rho/rhow-Chi*rhow2/rhow...
           alpha*rho/rhow-Chi];
    gammaP = roots(Eqn);                    
    gamma1 = gammaP(1); 
    gamma2 = gammaP(2);
    % Computing the proportionality constant of the shear wave
    gamma3 = -1*rhow/rhow2;                 Loops.vibration(ii,3)=gamma3;
    
    % Computing the wave numbers
    beta1 = ( (rho + gamma1*rhow)/(Chi + alpha*gamma1)/M * ohm^2)^(1/2);
    beta2 = ( (rho + gamma2*rhow)/(Chi + alpha*gamma2)/M * ohm^2)^(1/2);
    beta3 = ( (1 + gamma3*rhow/rho)*rho/LM * ohm^2)^(1/2);
    Loops.vibration(ii,6)=beta3;
    
    % Computing the wavelengths
    wl1 = 2*pi/real(beta1);                 
    wl2 = 2*pi/real(beta2);                 
    wl3 = 2*pi/real(beta3);                 Loops.vibration(ii,9)=wl3;
    
    % For consistency, beta1 should corrspond to the P1 wave, so its real
    % part should be smaller than that of beta2 (for the wavelength to be
    % larger). If this is not the case, some rearranging takes place before
    % storing the values into Loops.vibration.
    if abs(real(beta1)) < abs(real(beta2))  % they are well arranged
        Loops.vibration(ii,1)=gamma1;
        Loops.vibration(ii,2)=gamma2;
        Loops.vibration(ii,4)=beta1;
        Loops.vibration(ii,5)=beta2;
        Loops.vibration(ii,7)=wl1;
        Loops.vibration(ii,8)=wl2;
    else                                    % they are switched
        Loops.vibration(ii,1)=gamma2;
        Loops.vibration(ii,2)=gamma1;
        Loops.vibration(ii,4)=beta2;
        Loops.vibration(ii,5)=beta1;
        Loops.vibration(ii,7)=wl2;
        Loops.vibration(ii,8)=wl1;        
    end
    
end