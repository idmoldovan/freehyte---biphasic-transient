function [ohm,SpectralComponents,T] = ...
    GenTimeDepInput(TimeOrder)
% GENTIMEDEPINPUT generates the time-dependent input.
%
% GENTIMEDEPINPUT is called by MAIN***. It reads the input data inserted
% in  the GUIs and calculates the spectral frequencies and the force and
% displacement components for each spectral problem. Also, it builds the
% time basis T.
%
% ohm is a 2^TimeOrder vector containing the spectral frequencies.
% SpectralComponents is a matrix listing the projections of the
% time-dependent parts of the applied boundary conditions onto the wavelet
% space. SpectralComponents has as many lines as Dirichlet and Neumann
% boundaries and 2^TimeOrder+1 columns. In the first column, the index of
% the current boundary is stored.
% T is a matrix with the wavelet time basis. It has as 2^Ndya+1 lines and
% 2^TimeOrder columns. Each column corresponds to a wavelet function. Each
% line, to a dyadic time.
% 
% % BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Triphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1fL0Tj9x_RVJJJzialzf_wQMgG2uyWUBI
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developerís
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB2N2Q4cXZKcGc/view

% Adding the Wavelet folder to the path, in order to use its functions
addpath('./Wavelet');

% Loading information from the GUI
% time basis information
load('InputData','Dt','Nfam','Ndya','dataDir','dataNeu');

% Generating the wavelet basis
[PSI,T,ZH] = Generate_Basis(Nfam,TimeOrder,Ndya);

% Computing the spectral frequencies according to (2.74), reference [2]
ohm = -1.i*PSI*Dt^(-1);

% Generating the spectral components of the boundary conditions. They are
% stored in a matrix of dimensions (ExtBnds x 2^TimeOrder+1), where ExtBnds
% is the total number of external Dirichlet and Neumann boundaries, and the
% boundary ID is stored in the first column (so the spectral components are
% listed from column 2 to end).

% initialization
ExtBnds = 0;
if exist('dataDir')
    ExtBnds = ExtBnds + size(dataDir,1);
end
if exist('dataNeu')
    ExtBnds = ExtBnds + size(dataNeu,1);
end
SpectralComponents = zeros(ExtBnds,2^TimeOrder+1);

% if exterior Dirichlet boundaries exist
if exist('dataDir')
    for ii=1:size(dataDir,1)
        % registering the ID of the current boundary
        SpectralComponents(ii,1) = dataDir{ii,1};
        % getting the time variation function, on the [0,1] interval. The
        % scalar operators should first be replaced by vectorial ones.
        Ustr = strrep(strrep(strrep(dataDir{ii,5},'*','.*'),'/','./'),'^','.^');
        % it is now recalled that the time variation function input by the
        % user is  defined on the [0,Dt] interval. Therefore, it must be
        % mapped on a [0,1] interval using expression t -> t * Dt
        Ustr = strrep(Ustr,'t','t.*dt');
        % constructing the mapped time variation function
        timefunc = @(t,dt) eval(Ustr);
        % Generating the projection of the time variation function in the
        % wavelet basis
        projection = ZH*quadratura(Nfam,TimeOrder,timefunc,Dt).';
        % registering the spectral components of the current boundary
        SpectralComponents(ii,2:end) = projection.';
    end
else
    dataDir = []; % a dummy dataDir is needed for the Neumann boundaries
end

% if exterior Neumann boundaries exist
if exist('dataNeu')
    for ii=size(dataDir,1)+1:ExtBnds
        % registering the ID of the current boundary
        SpectralComponents(ii,1) = dataNeu{ii-size(dataDir,1),1};
        % getting the time variation function, on the [0,1] interval. The
        % scalar operators should first be replaced by vectorial ones.
        Ustr = strrep(strrep(strrep(dataNeu{ii-size(dataDir,1),5},'*','.*'),'/','./'),'^','.^');
        % it is now recalled that the time variation function input by the
        % user is  defined on the [0,Dt] interval. Therefore, it must be
        % mapped on a [0,1] interval using expression t -> t * Dt
        Ustr = strrep(Ustr,'t','t.*dt');
        % constructing the mapped time variation function
        timefunc = @(t,dt) eval(Ustr);
        % Generating the projection of the time variation function in the
        % wavelet basis
        projection = ZH*quadratura(Nfam,TimeOrder,timefunc,Dt).';
        % registering the spectral components of the current boundary
        SpectralComponents(ii,2:end) = projection.';
    end
end

% Removing the Wavelet folder to the path
rmpath('./Wavelet');

end




