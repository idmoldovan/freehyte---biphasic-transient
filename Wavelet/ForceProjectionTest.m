function [f,exact,err] = ForceProjectionTest(nfam,j0,pval)

% definition of the time variation function, on a [0,1] interval
timefunc = @(x,~) 2*x.*(x<=0.5);

% Generating the basis and the wavelet multipliers
[~,T,ZH] = Generate_Basis(nfam,j0,pval);
%[Tfile,ZHfile] = Generate_Basis_File(nfam,j0,pval);

% Generating the projection of the time variation function in the scale
% functions basis
projection = ZH*quadratura(nfam,j0,timefunc,1).';
%projectionfile = ZHfile*quadratura(nfam,j0,timefunc).';

% reconstruction of the original time function in the dyadic points
f = T*projection;
%ffile = Tfile*projectionfile;

% plotting the comparison between the approximated and actual force
xx = linspace(0,1,2^pval+1);
%plot(xx,timefunc(xx),xx,real(f),'*k',xx,real(ffile),'*r');
plot(xx,timefunc(xx),xx,real(f),'*k');

% computing the exact values of the force
exact = timefunc(xx).';

% computing the total error
err = sum(abs(timefunc(xx).'-real(f)))/sum(abs(timefunc(xx)));

end
