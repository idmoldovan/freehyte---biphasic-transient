 function [mat_xff]=operador_xff(nfam,j0)

% 
%  [mat_xff]=operador_xff(nfam,j0)
%
%  Rotina que efectua as integra�oes x*f*f

pval=12;
[rox]=int_xfe(nfam);

[H0,h0,G0,g0]=val_coef_left(nfam);
[H1,h1,G1,g1]=val_coef_right(nfam);
[h]=daubechies;

nloc=2^j0-2*nfam;

ndim=2^j0;

isalto=nloc-(2*nfam-2);

for ii=1:ndim
    for jj=1:ndim
        mat_xff(ii,jj)=0.0;
    end
end

[base_phi]=base_fe(nfam,j0,pval);
n_intervalos=2^pval;
npontos=2^pval+1;
delta=1.0/n_intervalos;

%
%  Calculo dos integrais envolvendo apenas fun�oes interiores
%
%for ii=1:nloc
%    for jj=1:nloc
%        clear vector;
%        for ip=1:npontos
%            vector(ip)=base_phi(ip,ii+nfam)*base_phi(ip,jj+nfam)*(ip-1)*delta;
%        end
%        matriz1(ii,jj)=trapz(vector)*delta;
%    end
%end

clear matriz2;       
for ii=1:nloc
    for jj=1:nloc
        ll=ii-jj+2*nfam-1;
        if ll >= 1 & ll <= 4*nfam-3
            matriz2(ii,jj)=rox(ll)/ndim;
        end
        if ii == jj
            matriz2(ii,jj)=matriz2(ii,jj)+(ii)/ndim;
        end
    end
end


%
% Calculo dos integrais envolvendo funcoes na extremidade esquerda e funcoes interiores
%
%
%for ii=1:nfam
%    for jj=1:nloc
%        clear vector;
%        for ip=1:npontos
%            vector(ip)=base_phi(ip,ii)*base_phi(ip,jj+nfam)*(ip-1)*delta;
%        end
%        matriz1(ii,jj)=trapz(vector)*delta;
%    end
%end

%matriz1


clear sis;
ntotal=nfam*(2*nfam-1);
sis=eye(ntotal);

for ii=1:ntotal
    ti(ii)=0.0;
    roFf(ii)=0.0;
end

for ii=0:nfam-1
    for ll=0:2*nfam-2
        ipos=(2*nfam-1)*ii+ll+1;
        for m=0:nfam-1
            for k=0:2*nfam-1
                ival=2*ll+k;
                if ival <= 2*nfam-2
                    ipos2=(2*nfam-1)*m+ival+1;
                    sis(ipos,ipos2)=sis(ipos,ipos2)-0.5*H0(ii+1,m+1)*h(k+1,nfam);
                end
            end
        end
        valor=0.0;
        for n=1:2*nfam-1
            for k=0:2*nfam-1
                iloc=2*ll+k-n;
                if iloc == 0
                    valor=valor+n/2*h0(ii+1,n)*h(k+1,nfam);
                end
                iloc=iloc+2*nfam-1;
                if iloc >=1 & iloc <= 4*nfam-3
                    valor=valor+h0(ii+1,n)*h(k+1,nfam)*0.5*rox(iloc);
                end
            end
        end
        ti(ipos)=valor;
    end
end

roFf=sis\ti';

for ii=1:nfam
    for jj=1:nloc
        matriz3(ii,jj)=0.0;
    end
end

for ii=0:nfam-1
    for jj=1:2*nfam-2
        iloc=(2*nfam-1)*ii+jj+1;
        if iloc >= 1 & iloc <= ntotal
            matriz3(ii+1,jj)=roFf(iloc)/2^j0;
        end
    end
end

matriz3;

%
% Calculo dos integrais envolvendo apenas funcoes na extremidade esquerda
%
%

%for ii=1:nfam
%    for jj=1:nfam
%        clear vector;
%        for ip=1:npontos
%            vector(ip)=base_phi(ip,ii)*base_phi(ip,jj)*(ip-1)*delta;
%        end
%        matriz1(ii,jj)=trapz(vector)*delta;
%    end
%end

%matriz1

ntotal=nfam*nfam;
ntotal2=nfam*(2*nfam-1);
clear sis;
clear ti;
sis=eye(ntotal);

for ii=1:nfam
    ti(ii)=0.0;
    roFiFi(ii)=0.0;
end

for ii=0:nfam-1
    for jj=0:nfam-1
        ipos=ii*nfam+jj+1;
        for m=0:nfam-1
            for p=0:nfam-1
                iloc2=m*nfam+p+1;
                sis(ipos,iloc2)=sis(ipos,iloc2)-0.5*H0(ii+1,m+1)*H0(jj+1,p+1);
            end
        end
        valor=0.0;
        for m=0:nfam-1
            for q=1:2*nfam-1
                if q <= 2*nfam-2
                    iloc=(2*nfam-1)*m+q+1;
                    valor=valor+H0(ii+1,m+1)*h0(jj+1,q)*0.5*roFf(iloc);
                end
            end
        end
        for n=1:2*nfam-1
            for p=0:nfam-1 
                if n <= 2*nfam-2
                    iloc=(2*nfam-1)*p+n+1;
                    valor=valor+H0(jj+1,p+1)*h0(ii+1,n)*0.5*roFf(iloc);
                end
            end
        end
        for n=1:2*nfam-1
            for q=1:2*nfam-1
                if n == q
                    valor =valor+q/2*h0(ii+1,n)*h0(jj+1,q);
                end
                iloc=+2*nfam-1+n-q;
                if iloc >= 1 & iloc <= 4*nfam-3
                    valor=valor+h0(jj+1,q)*h0(ii+1,n)*0.5*rox(iloc);
                end
            end
        end
        ti(ipos)=valor;
    end
end


roFiFi=sis\ti';

for ii=0:nfam-1
    for jj=0:nfam-1
        ipos=ii*nfam+jj+1;
        matriz4(ii+1,jj+1)=roFiFi(ipos)/2^j0;
    end
end

matriz4;

%
%
%  Calculo dos integrais envolvendo fun�oes interiores e fun�oes definidas no extremo direito do intervalo
%

isalto=2^j0-nfam;
for ii=1:nfam
    for jj=1:nloc
        clear vector;
        for ip=1:npontos
            vector(ip)=base_phi(ip,ii+isalto)*base_phi(ip,jj+nfam)*(ip-1)*delta;
        end
        matriz1(ii,jj)=trapz(vector)*delta;
    end
end

matriz1

clear sis;
ntotal=nfam*(2*nfam-1);
sis=eye(ntotal);

for ii=1:ntotal
    ti(ii)=0.0;
    roFfd(ii)=0.0;
end

for ii=0:nfam-1
    for ll=0:2*nfam-2
        ipos=(2*nfam-1)*ii+ll+1;
        vall=2-2*nfam-ll;
        for m=0:nfam-1
            for k=0:2*nfam-1
                ival=2*vall+k;
                if ival >= 4-4*nfam & ival <= 2-2*nfam
                    ival2=-ival-2*nfam+2;
                    ipos2=(2*nfam-1)*m+ival2+1;
                    sis(ipos,ipos2)=sis(ipos,ipos2)-0.5*H1(ii+1,m+1)*h(k+1,nfam);
                end
            end
        end
        valor=0.0;
        for n=1:2*nfam-1
            nn=2*nfam-1-n+1;
            for k=0:2*nfam-1
                iloc=2*vall+k-nn;
                if iloc == 0
                    valor=valor+nn*h1(ii+1,n)*h(k+1,nfam)*0.5;
                end
                iloc=iloc+2*nfam-1;
                iloc=-iloc;
                if iloc >=1 & iloc <= 4*nfam-3
                    valor=valor+h1(ii+1,n)*h(k+1,nfam)*0.5*rox(iloc);
                end
            end
        end
        ti(ipos)=valor;
    end
end

sis
ti

roFfd=sis\ti';

clear sis;
ntotal=nfam*(2*nfam-1);
sis=eye(ntotal);

for ii=1:ntotal
    ti(ii)=0.0;
    roFfd(ii)=0.0;
end

for ii=0:nfam-1
    for ll=0:2*nfam-2
        ipos=(2*nfam-1)*ii+ll+1;
        for m=0:nfam-1
            for k=0:2*nfam-1
                ival=2*ll+k;
                if ival <= 2*nfam-2
                    ipos2=(2*nfam-1)*m+ival+1;
                    sis(ipos,ipos2)=sis(ipos,ipos2)-0.5*H1(ii+1,m+1)*h(k+1,nfam);
                end
            end
        end
        valor=0.0;
        for n=1:2*nfam-1
            nn=2*nfam-1-n+1;
            for k=0:2*nfam-1
                iloc=2*ll+(k)-nn;
                if iloc == 0
                    valor=valor+nn/2*h1(ii+1,n)*h(2*nfam-k,nfam);
                end
                iloc=iloc+2*nfam-1;
                if iloc >=1 & iloc <= 4*nfam-3
                    valor=valor+h1(ii+1,n)*h(2*nfam-k,nfam)*0.5*rox(iloc);
                end
            end
        end
        ti(ipos)=valor;
    end
end

roFfd=sis\ti';
roFfd/2^j0

%for ii=1:nfam
%    for jj=1:nloc
%        matriz5(ii,jj)=0.0;
%    end
%end

%for ii=0:nfam-1
%    for jj=1:2*nfam-2
%        iloc=(2*nfam-1)*ii+jj+1;
%        if iloc >= 1 & iloc <= ntotal
%            matriz5(ii+1,jj)=roFf(iloc)/2^j0;
%        end
%    end
%end

