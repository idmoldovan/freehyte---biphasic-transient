 function [phi_left,psi_left,phi_right,psi_right]=val_int(nfam)
% 
%  [phi_left,psi_left,phi_right,psi_right]=val_int(nfam)
%
%  Fun��o que determina o valor que as fun��es de escala "� esquerda" tomam
%  em todos os inteiros existentes no seu intervalo de defini��o
%

[val_a,val_d]=daub_int(nfam);

[H0,h0,G0,g0]=val_coef_left(nfam);
[H1,h1,G1,g1]=val_coef_right(nfam);


n_inteiros=2*nfam;

phi_left(1:n_inteiros,1:nfam)=0.0;
psi_left(1:n_inteiros,1:nfam)=0.0;
phi_right(1:n_inteiros,1:nfam)=0.0;
psi_right(1:n_inteiros,1:nfam)=0.0;


for ip=n_inteiros:-1:2
   
   x_1_aux(1:nfam)=0.0;
   x_2_aux(1:2*nfam-1)=0.0;
   x_3_aux(1:nfam)=0.0;
   x_4_aux(1:2*nfam-1)=0.0;

   iponto=ip-1;
   iint=2*iponto;
   iloc=iint+1;
   if(iloc >=1 & iloc <= 2*nfam)
      for ifunc=1:nfam
         x_1_aux(ifunc)=phi_left(iloc,ifunc);
         x_3_aux(ifunc)=phi_right(iloc,ifunc);
      end
   end
   ifim=3*nfam-2;
   for ivar=nfam:ifim
      iloc=iint-ivar+nfam-1;
      if(iloc >= 1 & iloc <= 2*nfam-2)
         x_2_aux(ivar-nfam+1)=val_a(iloc);
         x_4_aux(ivar-nfam+1)=val_a(nfam+ivar-iint);
      end
   end
   
   phi_left(ip,1:nfam)=(sqrt(2)*(H0*x_1_aux'+h0*x_2_aux'))';
   psi_left(ip,1:nfam)=(sqrt(2)*(G0*x_1_aux'+g0*x_2_aux'))';
   phi_right(ip,1:nfam)=(sqrt(2)*(H1*x_3_aux'+h1*x_4_aux'))';
   psi_right(ip,1:nfam)=(sqrt(2)*(G1*x_3_aux'+g1*x_4_aux'))';


end

[V1,D1]=eig(H0*sqrt(2));
[V2,D2]=eig(H1*sqrt(2));

for i=1:nfam
   ival=sqrt((D1(i,i)-1)*(D1(i,i)-1));
   if (ival < 10^(-6))
      val_l_0=V1(1:nfam,i);
   end
   ival=sqrt((D2(i,i)-1)*(D2(i,i)-1));
   if (ival < 10^(-6))
      val_r_0=V2(1:nfam,i);
   end
end


sist1=eye(nfam);
sist2=eye(nfam);

for i=1:nfam
   for j=1:nfam
      sist1(i,j)=sist1(i,j)-H0(i,j)*sqrt(2.)/2.;
      sist2(i,j)=sist2(i,j)-H1(i,j)*sqrt(2.)/2.;
   end
end
for i=1:2*nfam-1
   ti_l(i)=sqrt(2)/2;
   ti_r(i)=sqrt(2)/2;
end

ti_l=h0*ti_l';
sol_l=inv(sist1)*ti_l;

ti_r=h1*ti_r';
sol_r=inv(sist2)*ti_r;



soma_l=sol_l(1);
soma_r=sol_r(1);

for i=2:nfam
   soma_l=soma_l+sol_l(i)*val_l_0(i)/val_l_0(1);
   soma_r=soma_r+sol_r(i)*val_r_0(i)/val_r_0(1);
end
phi_left(1,1)=1/soma_l;
phi_right(1,1)=1/soma_r;

for j=2:nfam
   phi_left(1,j)=phi_left(1,1)*val_l_0(j)/val_l_0(1);
   phi_right(1,j)=phi_right(1,1)*val_r_0(j)/val_r_0(1);
end

x_1_aux(1:nfam)=0.0;
x_2_aux(1:2*nfam-1)=0.0;
x_3_aux(1:nfam)=0.0;
x_4_aux(1:2*nfam-1)=0.0;

for ifunc=1:nfam
   x_1_aux(ifunc)=phi_left(1,ifunc);
   x_3_aux(ifunc)=phi_right(1,ifunc);
end
  
ifim=3*nfam-2;
for ivar=nfam:ifim
   iloc=-ivar+nfam-1;
   if(iloc >= 1 & iloc <= 2*nfam-2)
      x_2_aux(ivar-nfam+1)=val_a(iloc);
      x_4_aux(ivar-nfam+1)=val_a(nfam+ivar);
   end
end
   
psi_left(1,1:nfam)=(sqrt(2)*(G0*x_1_aux'+g0*x_2_aux'))';
psi_right(1,1:nfam)=(sqrt(2)*(G1*x_3_aux'+g1*x_4_aux'))';


return
