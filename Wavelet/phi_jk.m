 function [phi_x,dphi_x,psi_x,dpsi_x]=phi_jk(nfam,j,k,p)
%  
%  [phi_x,dphi_x,psi_x,dpsi_x]=phi_jk(nfam,j,k,p)
%
%  Rotina que determina o valor da translac��o k e dilata��o j da fun��o de 
%  escala, da wavelet prim�ria e das respectivas derivadas. 
%  Argumentos: nfam - Fam�lia de wavelets
%              j - Coeficiente de dilata��o
%              k - Coeficiente de translac��o
%              2^p - N�mero de intervalos por cada inteiro
%

ndimsis=2*nfam-2;

[h]=daubechies;

[val_a,val_d]=daub_int(nfam);

n_intervalos=2^p*(2*nfam-1);
npontos=2^p*(2*nfam-1)+1;

phi_x(1:npontos)=0.0;
dphi_x(1:npontos)=0.0;
psi_x(1:npontos)=0.0;
dpsi_x(1:npontos)=0.0;

x(1:npontos)=0.0;

iponto=1;
for i=1:ndimsis
   iponto=iponto+2^p;
   phi_x(iponto)=val_a(i);
   dphi_x(iponto)=val_d(i);
   soma1=0.0;
   soma2=0.0;
   for jj=0:2*nfam-1
      iloc=2*i-jj;
      if(iloc>=1 & iloc<= ndimsis)
         soma1=soma1+sqrt(2)*h(2*nfam-1-jj+1,nfam)*val_a(iloc)*(-1)^jj;
         soma2=soma2+2*sqrt(2)*h(2*nfam-1-jj+1,nfam)*val_d(iloc)*(-1)^jj;
      end
   end
   psi_x(iponto)=soma1;
   dpsi_x(iponto)=soma2;      
end



for ival=1:p
   ival;
   xstart=2^p/2^ival+1;
   incremento=2^(p-ival+1);
   for iponto=xstart:incremento:npontos
      soma=0.0;
      soma2=0.0;
      soma3=0.0;
      soma4=0.0;
      iponto;
      for jj=0:2*nfam-1
         iloc=iponto*2-1-jj*2^p;
         if(iloc <=npontos & iloc >=1)
            soma=soma+sqrt(2)*h(jj+1,nfam)*phi_x(iloc);
            soma2=soma2+2*sqrt(2)*h(jj+1,nfam)*dphi_x(iloc);
            soma3=soma3+sqrt(2)*h(2*nfam-1-jj+1,nfam)*phi_x(iloc)*(-1)^jj;
            soma4=soma4+2*sqrt(2)*h(2*nfam-1-jj+1,nfam)*dphi_x(iloc)*(-1)^jj;
         end
      end
      phi_x(iponto)=soma;
      dphi_x(iponto)=soma2;
      psi_x(iponto)=soma3;
      dpsi_x(iponto)=soma4;

   end
end

delta=2^(-p);
x(1)=0.0;
for i=2:npontos
   x(i)=x(i-1)+delta;
end

x=(x+k)/2^j;
phi_x=phi_x*2^(j/2);
dphi_x=dphi_x*2^(3*j/2);
psi_x=psi_x*2^(j/2);
dpsi_x=dpsi_x*2^(3*j/2);
%
% Representacao grafica das funcoes geradas
%
%figure
%plot(x,phi_x)
%figure
%plot(x,dphi_x)
%figure
%plot(x,psi_x)
%figure
%plot(x,dpsi_x)

