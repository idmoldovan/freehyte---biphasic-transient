 function [val_a,val_d]=daub_int(nfam)
% 
%  [val_a,val_d]=daub_int(nfam)
%
%  Função que determina o valor que a função de escala e a sua derivada tomam
%  em todos os inteiros existentes no seu intervalo de definição
%

%% Inicializações

[h]=daubechies;

ndimsis=2*nfam-2;
ilim=2*nfam-1;

vvpsis(1:ndimsis,1:ndimsis)=0;


%% Definição do problema de valores e vectores próprios

for i=1:ndimsis
   for j=1:ndimsis
      index=2*i-j;
      if (index >= 0 && index <= ilim)
         vvpsis(i,j)=h(index+1,nfam)*sqrt(2.0);
      else
         vvpsis(i,j)=0.0;
      end
   end
end


%% Determinação dos valores das funções de escala e suas derivadas

[V,D]=eig(vvpsis);

for i=1:ndimsis
   ival=sqrt((D(i,i)-1)*(D(i,i)-1));
   jval=sqrt((D(i,i)-0.5)*(D(i,i)-0.5));
   if (ival < 10^(-6))
      val_a=V(1:ndimsis,i);
   end
   if (jval < 10^(-6))
      val_d=V(1:ndimsis,i);
   end
end

soma1=0.0;
soma2=0.0;

for i=1:ndimsis
   soma1=soma1+val_a(i);
   soma2=soma2-val_d(i)*i;
end

val_a=val_a/soma1;
val_d=val_d/soma2;

return
