function [PSI,T,ZH] = Generate_Basis(nfam,j0,pval)
% GENERATE_BASIS generates the wavelet time basis and the related matrices
% required by the formulation. 
%
% Currently, the time basis is built on Daubechies scaling functions of
% family NFAM, refinement J0, and 2^PVAL+1 dyadic points.
%
% GENERATE_BASIS is called by GENTIMEINPUT and returns vector PSI, time
% basis matrix T and projection matrix ZH. 
% PSI is a 2^J0 vector containing the diagonal elements of the PSI
% matrix that enables the uncoupling of the spectral problems.
% T is a matrix with the wavelet time basis. It has as 2^PVAL+1 lines and
% 2^J0 columns. Each column corresponds to a wavelet function. Each
% line, to a dyadic time.
% ZH is a square matrix (2^J0) used for the projection of the
% time-dependent functions in the wavelet space.
%
% BIBLIOGRAPHY
% 1. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 
% The wavelet basis routines BASE_FE & OPERADOR_FE are due to Lu�s Castro.  
% Many thanks to Lu�s! The basis generation is detailed in Appendix A of
% reference [1].

% Generate the Daubechies scale functions basis in the dyadic points
base_phi = base_fe(nfam,j0,pval); 

% Generate the inner products between the scale functions and its
% derivatives (needed for expression (A.5) in [1])
mat_derivada=operadord_fe(nfam,j0)';

% Generates matrix h, defined by expression (A.4) in reference [1]. Since
% the Daubechies basis is orthonormal, h is the identity matrix
h = eye(2^j0);

% Generates matrix g, defined by expression (A.5) in reference [1].
g = base_phi(end,:)'*base_phi(end,:)-mat_derivada;

% Eigenanalysis of h. For the particular case of the (orthonormal)
% Daubechies wavelets, both E and Lambda are the unit matrix.
[E,Lambda] = eig(h);
%E = fliplr(E);

% Generates matrix g*, defined by expression (A.8) in reference [1].
gstar = E*g*E;

% Eigenvalues and eigenvectors of g*
[Estar,PSI]=eig(gstar);
PSI=diag(PSI);

% Generates matrix Z, defined by expression (A.8) in reference [1]. Estar
% should have been transposed in that expression.
Z = Estar.'*E.';

% Basis T corresponds to w in reference [1], calculated according to
% expression (A.1)
T = base_phi*Z.';

% Inverse of the H matrix
% Hm1 = inv(Estar'*Estar);

% Generates the ZH matrix, defined as the multiplier of phi(0) in
% expression (A.11) of reference [1].
ZH = (Estar'*Estar)\conj(Z);

% Computing psi, according to expression (A.11) of reference [1].
psi = ZH*base_phi(1,:).';

end
