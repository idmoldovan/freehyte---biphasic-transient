 function [dphi_right_x,dpsi_right_x]=dphi_right_j(nfam,j,p)
%  
%  [dphi_right_x,dpsi_right_x]=dphi_right_j(nfam,j,p)
%
%  Rotina que determina o valor da dilatação j das funções de 
%  escala e das wavelets definidas em 0. 
%  Argumentos: nfam - Família de wavelets
%              j - Coeficiente de dilatação
%              2^p - Número de intervalos por cada inteiro
%
%
%% Inicializações


[~,~,dphi_right,dpsi_right]=dval_int(nfam);

[~,dphi_x,~,~]=phi_jk(nfam,0,0,p);

[H1,h1,G1,g1]=val_coef_right(nfam);

npontos=2^p*(2*nfam-1)+1;

dphi_right_x(1:npontos,1:nfam)=0.0;
dpsi_right_x(1:npontos,1:nfam)=0.0;


%% Determinação das funções solicitadas

iponto=1;
for i=1:2*nfam
   for ifunc=1:nfam
      dphi_right_x(iponto,ifunc)=dphi_right(i,ifunc);
      dpsi_right_x(iponto,ifunc)=dpsi_right(i,ifunc);
   end
      iponto=iponto+2^p;
end

for ival=1:p
   xstart=(2^p)/(2^ival)+1;
   incremento=2^(p-ival+1);
   for iponto=xstart:incremento:npontos
      x_1_aux(1:nfam)=0.0;
      x_2_aux(1:2*nfam-1)=0.0;
      for ii=1:nfam
         iloc=iponto*2-1;
         if(iloc>=1 && iloc <= npontos)
            x_1_aux(ii)=dphi_right_x(iloc,ii);
         end
      end
      for jj=nfam:3*nfam-2
         ivar=jj-nfam+1;
         iloc=iponto*2-1-(ivar)*2^p;
         if(iloc <=npontos && iloc >=1)
            x_2_aux(ivar)=dphi_x(npontos-iloc+1);
         end
      end
      dphi_right_x(iponto,1:nfam)=(2*sqrt(2)*(H1*x_1_aux'+h1*x_2_aux'))';
      dpsi_right_x(iponto,1:nfam)=(2*sqrt(2)*(G1*x_1_aux'+g1*x_2_aux'))';
   end
end

dphi_right_x=dphi_right_x*2^(3*j/2);
dpsi_right_x=dpsi_right_x*2^(3*j/2);

%
% Traçado dos graficos
%
%x(1:npontos)=0.0;
%delta=2^(-p);
%x(1)=1.0;
%for i=2:npontos
%   x(i)=x(i-1)-delta;
%end
%x=x/2^j;

%val=2
%for ip=1:nfam
%   val=val+1;
%   figure(val);
%   plot(x,phi_right_x(1:npontos,ip));
%   figure(20+val)
%   plot(x,psi_right_x(1:npontos,ip));
%end

