 function [rox]=int_xfe(nfam)
% 
%  [rox]=int_xfe(nfam)
%
%  Função que determina o valor dos integrais envolvendo produtos de funções de escala
%  com o polinomio x.

[h]=daubechies;

%%
% Inicialização de algumas variaveis
%
ilim=2*nfam-1;

inic_l=2-2*nfam;

ifim_l=2*nfam-2;

numero_l=ifim_l-inic_l+1;

sis=eye(numero_l);
  
for ii=1:numero_l
    ti(ii)=0.0;
end

%% Determinação dos valores dos integrais

for l=1:numero_l
    val_l=l-ilim;
    for ii=0:ilim
        for jj=0:ilim 
            ipos=2*val_l+ii-jj;
            if ipos == 0
                ti(l)=ti(l)+jj*sqrt(2)*h(ii+1,nfam)*sqrt(2)*h(jj+1,nfam)/4;
            end
            ipos=ipos+ilim;
            if ipos>0 & ipos < numero_l+1
                sis(l,ipos)=sis(l,ipos)-sqrt(2)*h(ii+1,nfam)*sqrt(2)*h(jj+1,nfam)/4;
            end
        end
    end
end

rox=sis\ti';
