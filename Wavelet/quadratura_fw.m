 function [val_int]=quadratura_fw(nfam,j0,jmax)
% 
%  [val_int]=quadratura_fw(nfam,j0,jmax)
%
%  Fun��o que determina o valor dos integrais aplicando as regras de quadratura
%  Considera-se que a base �e constituida por funcoes de escala e wavelets

% Inicializa�oes

jtotal=jmax+1;
ntransform=jmax-j0;

ndim1=2^jtotal;
ndim2=2^jmax;

for ii=1:ndim1
    val_int=0.0;
end

[val_int]=quadratura(nfam,jtotal);

[mat_P1,mat_P2]=projectores(nfam,jtotal);

Bloco1=mat_P1*val_int';
Bloco2=mat_P2*val_int';

for ii=1:ndim2
    val_int(ii)=Bloco1(ii);
    val_int(ii+ndim2)=Bloco2(ii);
end

jacumula=0;

for itr=1:ntransform
    
    jota=jmax-itr+1;
    
    ndim1=2^jota;
    ndim2=2^(jota-1);
   
    clear Bloco1;
    clear Bloco2;
    clear mat_P1;
    clear mat_P2;
    
    [mat_P1,mat_P2]=projectores(nfam,jota);
    
    for ii=1:ndim1
        vec_aux(ii)=val_int(ii);
    end
    
    Bloco1=mat_P1*vec_aux';
    Bloco2=mat_P2*vec_aux';
    
    for ii=1:ndim2
        val_int(ii)=Bloco1(ii);
        val_int(ii+ndim2)=Bloco2(ii);
    end
    
    clear vec_aux;
    
end

val_int