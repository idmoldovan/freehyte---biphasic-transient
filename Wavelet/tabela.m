fid = fopen('tabela.f','w','a');


for N=2:10
   [H0,h0,G0,g0]=val_coef_left(N);
   [H1,h1,G1,g1]=val_coef_right(N);
   
   fprintf(fid,'\n\nN=%i\n\n',N);
   
for i=1:N
   for j=1:N
      fprintf(fid,'      H00(%i,%i)=%14.10e\n',i,j,H0(i,j));
   end
end

   fprintf(fid,'\n\n');

for i=1:N
   for j=1:2*N-1
      fprintf(fid,'      H01(%i,%i)=%14.10e\n',i,j,h0(i,j));
   end
end
   fprintf(fid,'\n\n');
for i=1:N
   for j=1:N
      fprintf(fid,'      G00(%i,%i)=%14.10e\n',i,j,G0(i,j));
   end
end
   fprintf(fid,'\n\n');

for i=1:N
   for j=1:2*N-1
      fprintf(fid,'      G01(%i,%i)=%14.10e\n',i,j,g0(i,j));
   end
end
   fprintf(fid,'\n\n');

for i=1:N
   for j=1:N
      fprintf(fid,'      H10(%i,%i)=%14.10e\n',i,j,H1(i,j));
   end
end

   fprintf(fid,'\n\n');

for i=1:N
   for j=1:2*N-1
      fprintf(fid,'      H11(%i,%i)=%14.10e\n',i,j,h1(i,j));
   end
end
   fprintf(fid,'\n\n');

for i=1:N
   for j=1:N
      fprintf(fid,'      G10(%i,%i)=%14.10e\n',i,j,G1(i,j));
   end
end

   fprintf(fid,'\n\n');

for i=1:N
   for j=1:2*N-1
      fprintf(fid,'      G11(%i,%i)=%14.10e\n',i,j,g1(i,j));
   end
end




end





fclose(fid);
