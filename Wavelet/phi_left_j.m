 function [phi_left_x,psi_left_x]=phi_left_j(nfam,j,p)
%  
%  [phi_left_x,psi_left_x]=phi_left_j(nfam,j,p)
%
%  Rotina que determina o valor da dilata��o j das fun��es de 
%  escala e das wavelets definidas em 0. 
%  Argumentos: nfam - Fam�lia de wavelets
%              j - Coeficiente de dilata��o
%              2^p - N�mero de intervalos por cada inteiro
%



[val_a,val_d]=daub_int(nfam);

[phi_left,psi_left,phi_right,psi_right]=val_int(nfam);

[phi_x,dphi_x,psi_x,dpsi_x]=phi_jk(nfam,0,0,p);

[H0,h0,G0,g0]=val_coef_left(nfam);

n_intervalos=2^p*(2*nfam-1);
npontos=2^p*(2*nfam-1)+1;

phi_left_x(1:npontos,1:nfam)=0.0;
psi_left_x(1:npontos,1:nfam)=0.0;
x(1:npontos)=0.0;

iponto=1;
for i=1:2*nfam
   for ifunc=1:nfam
      phi_left_x(iponto,ifunc)=phi_left(i,ifunc);
      psi_left_x(iponto,ifunc)=psi_left(i,ifunc);
   end
      iponto=iponto+2^p;
end

for ival=1:p
   xstart=(2^p)/(2^ival)+1;
   incremento=2^(p-ival+1);
   for iponto=xstart:incremento:npontos
      soma=0.0;
      iponto;
      x_1_aux(1:nfam)=0.0;
      x_2_aux(1:2*nfam-1)=0.0;
      for ii=1:nfam
         iloc=iponto*2-1;
         if(iloc>=1 & iloc <= npontos)
            x_1_aux(ii)=phi_left_x(iloc,ii);
         end
      end
      for jj=nfam:3*nfam-2
         ivar=jj-nfam+1;
         iloc=iponto*2-1-(ivar)*2^p;
         if(iloc <=npontos & iloc >=1)
            x_2_aux(ivar)=phi_x(iloc);
         end
      end
      phi_left_x(iponto,1:nfam)=(sqrt(2)*(H0*x_1_aux'+h0*x_2_aux'))';
      psi_left_x(iponto,1:nfam)=(sqrt(2)*(G0*x_1_aux'+g0*x_2_aux'))';
   end
end

delta=2^(-p);
x(1)=0.0;
for i=2:npontos
   x(i)=x(i-1)+delta;
end

x=x/2^j;
phi_left_x=phi_left_x*2^(j/2);
psi_left_x=psi_left_x*2^(j/2);

%
% Tra�ado de graficos
%
%val=2
%for ip=1:nfam
%   val=val+1;
%   figure;
%   plot(x,phi_left_x(1:npontos,ip));
%   figure
%   plot(x,psi_left_x(1:npontos,ip));
%end

