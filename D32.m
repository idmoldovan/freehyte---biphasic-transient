function [Dline, Dcol, Dval, CrtLineD] = D32(Edges, Loops, Dline, Dcol, Dval, CrtLineD, abscissa, weight)
% D32 sweeps through the elements and calls the functions that generate the
% D32 block of the dynamic matrix in the LHS.
%
% D32 is called by MAIN***. It receives as input data the Edges and Loops
% structures, the LHS matrix (that is, the matrix of coefficients of the
% solving system), and the Gauss-Legendre integration parameters abscissa
% and weights. It returns to MAIN*** the LHS matrix with the D32 blocks of
% all elements inserted at the correct positions (as determined in
% ASSIGNPARTS).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developerís 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB2N2Q4cXZKcGc/view
%
%
% D32 computes the internal product between the following bases, expressed
% in a polar (r,th) referential:
% * the displacement basis U3, 
%   U3 = | Ur | = 1/2 * | i*beta_s^-1*(J(n+1,z_s)+J(n-1,z_s))         | * exp(i*n*th)
%        | Ut |         | beta_s^-1*(J(n+1,z_s)-J(n-1,z_s))           |
%        | Wr |         | i*gamma_s*beta_s^-1*(J(n+1,z_s)+J(n-1,z_s)) |
%        | Wt |         | gamma_s*beta_s^-1*(J(n+1,z_s)-J(n-1,z_s))   |
% * the boundary traction basis N * S2, where S2 is the stress basis
%   S2 = | Sr | = 1/2 * | LM*(2*J(m,z_p2)+J(m-2,z_p2)+J(m+2,z_p2))-2*(Chi+gamma_p2*alpha)*M*J(m,z_p2) | *exp(i*m*th)
%        | St |         | LM*(2*J(m,z_p2)-J(m-2,z_p2)-J(m+2,z_p2))-2*(Chi+gamma_p2*alpha)*M*J(m,z_p2) |
%        | Srt|         | i*LM*(J(m-2,z_p2)-J(m+2,z_p2))                                              |
%        | Pi |         | -2*(alpha+gamma_p2)*M*J(m,z_p2)                                             |
%   and N is the directory cosine matrix,
%         N = | nr  0  nt  0  |
%             | 0  nt  nr  0  |
%             | 0   0   0  nr |
%             | 0   0   0  nt |
% In the above expressions, n and m are the line and column of the current 
% term in matrix D32; LM, Chi, M, alpha are material properties, beta_p2/s
% and gamma_p2/s are vibration properties of the P2/S wave (see
% INPUTPROC); z_p2/s = beta_p2/s*r; J(n,z) is the Bessel function of the
% first kind, order n and argument z; and nr and nt are the radial and
% tangential components of the outward unit normal to the current boundary.
%
% As typical of the Trefftz method, the displacement and stress bases solve
% exactly the equilibrium, compatibility and elasticity equations in the
% domain of each finite element. As a direct consequence, the dynamic
% matrix can be calculated using boundary integrals only.
%
% The derivation of the bases from the solution of the (free-field) Navier
% equation is presented in reference [2].

%% Sweeping the elements

for ii=1:length(Loops.area)
    
    % LocLoop is a local structure where the features of the current
    % element which are directly useful for the calculation of the
    % dynamic block are stored.
    LocLoop = struct('id',ii,'edges',Loops.edges(ii,:), 'center',...
        Loops.center(ii,:),'order',Loops.order(ii),... 
        'insert',Loops.insert(ii,:),'dim',Loops.dim(ii,:),...
        'materials',Loops.materials(ii,:),...
        'vibration',Loops.vibration(ii,:));
    
    % Computing the D32 matrix of element ii. Function D32_MATRIX_I is a
    % local function (see below).
    D32i = D32_Matrix_i(LocLoop, Edges, abscissa, weight);
    
    % getting the sparse vectors from D32i
    [LD, CD, VD] = find(D32i);
    
    % Inserting the matrix in the D sparse vectors.
    Dline(CrtLineD:CrtLineD+length(LD)-1) = LocLoop.insert(3)+LD-1;
    Dcol(CrtLineD:CrtLineD+length(CD)-1) = LocLoop.insert(2)+CD-1;
    Dval(CrtLineD:CrtLineD+length(VD)-1) = VD;
    
    CrtLineD = CrtLineD + length(LD);
    
end

end

function D32i = D32_Matrix_i(LocLoop, Edges, abscissa, weight)
% D32_MATRIX_I local function computes the D32 dynamic block of the 
% LocLoop element. The sides are mapped to a [-1,1] interval to perform the 
% integrations.

%% Initialization 
% Initialization of the D32 block
D32i = zeros(LocLoop.dim(3),LocLoop.dim(2));
% Loading material properties
alpha = LocLoop.materials(3);
MM = LocLoop.materials(4); % Biot's module
LM = LocLoop.materials(11);
Chi = LocLoop.materials(14);
% Loading vibration properties
gamma2 = LocLoop.vibration(2);
gamma3 = LocLoop.vibration(3);
beta2 = LocLoop.vibration(5);
beta3 = LocLoop.vibration(6);

% n + LocLoop.order + 1 is the current line; 
% m + LocLoop.order + 1 is the current column.
n = -LocLoop.order:LocLoop.order;
m = -LocLoop.order:LocLoop.order;

% Sweeping the edges for contour integration
for jj = 1:length(LocLoop.edges)  
    
    % identification of the jj-th edge of the loop
    id = LocLoop.edges(jj);  
    
    % LocEdge is a local structure where the features of the current
    % edge which are directly useful for the calculation of the
    % dynamic block are stored.    
    LocEdge =  struct('id',id,'nini',Edges.nini(id), 'nfin',Edges.nfin(id),...
        'parametric',Edges.parametric(id,:),'lleft',Edges.lleft(id),...
        'lright',Edges.lright(id));
    
    %% Generating the geometric data
    % The following code transforms the abscissa coordinates, expressed in
    % the [-1,1] referential, to the polar coordinates required to compute
    % the values of the basis functions. The components of the outward
    % normal to the boundary in the radial and tangential directions are
    % also calculated. They are required to transform the stress basis into
    % the traction basis.
    
    % Computing the length of the current edge
    L = sqrt(LocEdge.parametric(3)^2 + LocEdge.parametric(4)^2); 
    
    % Constructing the 3D matrices containing the n x m x abscissa
    % integration grid
    [N,M,A] = ndgrid(n,m,abscissa);
    
    % Transforming the edge abscissa into local coordinates. The local
    % referential is centered in the barycenter of the element, its axes
    % aligned with the Cartesian axes of the global referential.
    loc_x = LocEdge.parametric(1) - LocLoop.center(1) + 0.5 *...
        (A + 1) * LocEdge.parametric(3);  
    loc_y = LocEdge.parametric(2) - LocLoop.center(2) + 0.5 *...
        (A + 1) * LocEdge.parametric(4); 
    
    % Transforming the local Cartesian coordinates into polar.
    R = sqrt(loc_x.^2 + loc_y.^2);  
    T = atan2(loc_y, loc_x);
    
    % Computing the components of the outward normal in the Cartesian
    % directions.
    nx = LocEdge.parametric(4) / L;
    ny = -1* LocEdge.parametric(3) / L;
    if LocEdge.lright==LocLoop.id  % if the element is on the right,
        nx = -nx;                  % changes the sign of the normal
        ny = -ny;
    end
 
    % Computing the components of the outward normal in the polar
    % directions.
    NR = nx * cos(T) + ny * sin(T);   
    NT = -1*nx * sin(T) + ny * cos(T);
    
    
    %% Computing the basis functions for all integration points
    % Polar components of the displacement basis 
    Ur = 1i*beta3^-1 * (besselj(N+1,beta3*R) + besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    Ut = beta3^-1 * (besselj(N+1,beta3*R) - besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    Wr = 1i*gamma3*beta3^-1 * (besselj(N+1,beta3*R) + besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    Wt = gamma3*beta3^-1*(besselj(N+1,beta3*R) - besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    % Polar components of the stress basis
    Sr = ( LM*(2*besselj(M,beta2*R)+besselj(M-2,beta2*R)+...
        besselj(M+2,beta2*R)) - 2*(Chi+gamma2*alpha)*MM*besselj(M,beta2*R) )...
        .* exp(1i*M.*T);
    St = ( LM*(2*besselj(M,beta2*R)-besselj(M-2,beta2*R)-...
        besselj(M+2,beta2*R)) - 2*(Chi+gamma2*alpha)*MM*besselj(M,beta2*R) )...
        .* exp(1i*M.*T);
    Srt = 1i*LM* ( besselj(M-2,beta2*R)-besselj(M+2,beta2*R) )...
        .* exp(1i*M.*T);
    Pi = -2*(alpha+gamma2)*MM*besselj(M,beta2*R)...
        .* exp(1i*M.*T);
    
    %% Computing the integral on the side
    % The integral is the internal product between the displacement basis U
    % and the traction basis N * S
    
    NUS = NR.*(conj(Ur).*Sr+conj(Ut).*Srt+conj(Wr).*Pi) + ...
          NT.*(conj(Ur).*Srt+conj(Ut).*St+conj(Wt).*Pi);
    
    % Performing the side integration and updating the D32 matrix
    w3D(1,1,:) = weight;
    % computes the integral. '1/4 = 1/2*1/2' is the product of the shape
    % functions' multipliers that are not included in the expressions above
    D32i = D32i + 1/4*L/2 * sum(bsxfun(@times,NUS,w3D),3); 
 
end

end