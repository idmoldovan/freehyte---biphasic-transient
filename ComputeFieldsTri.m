function [LocResults] = ComputeFieldsTri(NoDiv,Nodes,Loops,Scales,X)
% COMPUTEFIELDSTRI computes and stores the spectral displacement and
% stress fields in the structure LOCRESULTS.
%
% COMPUTEFIELDSTRI is called by MAINTRI. It receives structures Loops and Scales, the
% solution vector X (calculated in MAINTRI), the node position list Node
% and the number of divisions for plotting in the Cartesian directions,
% NoDiv (by default defined as equal to the number of Gauss-Legendre
% integration points, see INPUTPROC).
% It returns the cell of structures LOCRESULTS, containing the
% (8) spectral fields Ux, Uy, Wx, ..., Pi computed in a grid of (NoDiv+1)^2
% (Gauss) points in every element. This means, Ux, Uy, ..., Pi
% are composed of as many cells as finite elements, and in each cell we
% have a (NoDiv+1 x NoDiv+1) matrix of complex numbers.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developerís 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% The estimates of the spectral displacement and stress fields are obtained
% by substituting the solution X of the solving system in the domain
% approximations of the respective fields. This is done element by element.
% The estimates are used for computing the time variations of the
% displacement and stress fields after multiplication by the time basis.
%
% The computation of the field estimates is further covered reference [2].


%% Sweeping the elements to compute the solution fields 
for ii=1:length(Loops.area) 

    % LocLoop is a local structure where the features of the current
    % element which are directly useful for the calculation of the output
    % fields are stored.
    LocLoop = struct('id',ii,'nodes',Loops.nodes(ii,:),'center',...
        Loops.center(ii,:),'order',Loops.order(ii),'insert',...
        Loops.insert(ii,:),'dim',Loops.dim(ii,:),...
        'materials',Loops.materials(ii,:),...
        'vibration',Loops.vibration(ii,:));

    % Loading material properties
    alpha = LocLoop.materials(3);
    MM = LocLoop.materials(4); % Biot's module
    LM = LocLoop.materials(11);
    Chi = LocLoop.materials(14);
    
    % Extracting vibration features
    gamma1 = LocLoop.vibration(1);
    gamma2 = LocLoop.vibration(2);
    gamma3 = LocLoop.vibration(3);
    beta1 = LocLoop.vibration(4);
    beta2 = LocLoop.vibration(5);
    beta3 = LocLoop.vibration(6);
    
    % Vector containing the orders of the basis
    n(1,1,:) = -LocLoop.order:LocLoop.order; 
    
    %% Generating the geometric data
    % Getting coordinates of the nodes of the element (global). 
    LocNodes = Nodes(LocLoop.nodes(:),:);
    
    % Generating the plotting points in the global Cartesian referential.
    % The plotting points belong to the Gauss-Legendre quadrature of the
    % triangular element. 
    [GlobalX,GlobalY,~,~]=triquad(NoDiv+1,LocNodes);
    % Generating the output grid in local coordinates.
    x = GlobalX - LocLoop.center(1);
    y = GlobalY - LocLoop.center(2);
    % Transforming the local Cartesian coordinates into polar.
    r = sqrt(x.^2 + y.^2);  
    t = atan2(y,x);
    % Generating the 3D matrices, for consistency with the programming
    % strategy used in the regular meshes.
    R = repmat(r,[1 1 length(n)]);
    T = repmat(t,[1 1 length(n)]);
    N = repmat(n,[NoDiv+1 NoDiv+1 1]);
    
    %% Computing the basis functions
    % Computing the displacement shape functions, in the polar referential.
    % For a full description of the basis, please refer to reference [2].
    % U1 basis
    U1r = 1/2 * beta1^-1 * (besselj(N-1,beta1*R) - besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    U1t = 1/2 * 1i * beta1^-1 * (besselj(N-1,beta1*R) + besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    W1r = 1/2 * gamma1*beta1^-1 * (besselj(N-1,beta1*R) - besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    W1t = 1/2 * gamma1*1i*beta1^-1*(besselj(N-1,beta1*R) + besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    % U2 basis             
    U2r = 1/2 * beta2^-1 * (besselj(N-1,beta2*R) - besselj(N+1,beta2*R))...
        .* exp(1i*N.*T);
    U2t = 1/2 * 1i * beta2^-1 * (besselj(N-1,beta2*R) + besselj(N+1,beta2*R))...
        .* exp(1i*N.*T);
    W2r = 1/2 * gamma2*beta2^-1 * (besselj(N-1,beta2*R) - besselj(N+1,beta2*R))...
        .* exp(1i*N.*T);
    W2t = 1/2 * gamma2*1i*beta2^-1*(besselj(N-1,beta2*R) + besselj(N+1,beta2*R))...
        .* exp(1i*N.*T);
    % U3 basis
    U3r = 1/2 * 1i*beta3^-1 * (besselj(N+1,beta3*R) + besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    U3t = 1/2 * beta3^-1 * (besselj(N+1,beta3*R) - besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    W3r = 1/2 * 1i*gamma3*beta3^-1 * (besselj(N+1,beta3*R) + besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    W3t = 1/2 * gamma3*beta3^-1*(besselj(N+1,beta3*R) - besselj(N-1,beta3*R))...
        .* exp(1i*N.*T);
    
    % Computing the stress shape functions, in the polar referential.
    % For a full description of the basis, please refer to reference [2].
    % S1 basis
    S1r = 1/2 * ( LM*(2*besselj(N,beta1*R)+besselj(N-2,beta1*R)+...
        besselj(N+2,beta1*R)) - 2*(Chi+gamma1*alpha)*MM*besselj(N,beta1*R) )...
        .* exp(1i*N.*T);
    S1t = 1/2 * ( LM*(2*besselj(N,beta1*R)-besselj(N-2,beta1*R)-...
        besselj(N+2,beta1*R)) - 2*(Chi+gamma1*alpha)*MM*besselj(N,beta1*R) )...
        .* exp(1i*N.*T);
    S1rt = 1/2 * 1i*LM* ( besselj(N-2,beta1*R)-besselj(N+2,beta1*R) )...
        .* exp(1i*N.*T);
    Pi1 = 1/2 * -2*(alpha+gamma1)*MM*besselj(N,beta1*R)...
        .* exp(1i*N.*T);
    % S2 basis
    S2r = 1/2 * ( LM*(2*besselj(N,beta2*R)+besselj(N-2,beta2*R)+...
        besselj(N+2,beta2*R)) - 2*(Chi+gamma2*alpha)*MM*besselj(N,beta2*R) )...
        .* exp(1i*N.*T);
    S2t = 1/2 * ( LM*(2*besselj(N,beta2*R)-besselj(N-2,beta2*R)-...
        besselj(N+2,beta2*R)) - 2*(Chi+gamma2*alpha)*MM*besselj(N,beta2*R) )...
        .* exp(1i*N.*T);
    S2rt = 1/2 * 1i*LM* ( besselj(N-2,beta2*R)-besselj(N+2,beta2*R) )...
        .* exp(1i*N.*T);
    Pi2 = 1/2 * -2*(alpha+gamma2)*MM*besselj(N,beta2*R)...
        .* exp(1i*N.*T);
    % S3 basis
    S3r = 1/2 * 1i*LM*(besselj(N-2,beta3*R)-besselj(N+2,beta3*R)).* exp(1i*N.*T);
    S3t = 1/2 * -1i*LM*(besselj(N-2,beta3*R)-besselj(N+2,beta3*R)).* exp(1i*N.*T);
    S3rt = 1/2 * -LM*(besselj(N-2,beta3*R)+besselj(N+2,beta3*R)).* exp(1i*N.*T);
    Pi3 = 0;
    
    % Extracting the multipliers of each part of the basis from the X
    % solution vector
    X1(1,1,:) = X(LocLoop.insert(1):LocLoop.insert(1)+LocLoop.dim(1)-1);
    X2(1,1,:) = X(LocLoop.insert(2):LocLoop.insert(2)+LocLoop.dim(2)-1);
    X3(1,1,:) = X(LocLoop.insert(3):LocLoop.insert(3)+LocLoop.dim(3)-1);
%     X1(1,1,:) = 0;                                                                                %  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
%     X2(1,1,:) = 0;
    %     X3(1,1,:) = 0;
    
    %% Computing the displacement and stress fields
    % Computing the displacement and stress fields in the polar referential.
    % They are the product of the basis functions with the corresponding
    % solution vectors.
    Ur = sum(bsxfun(@times,U1r,X1),3) + sum(bsxfun(@times,U2r,X2),3) + ...
        sum(bsxfun(@times,U3r,X3),3); 
    
    Ut = sum(bsxfun(@times,U1t,X1),3) + sum(bsxfun(@times,U2t,X2),3) + ...
        sum(bsxfun(@times,U3t,X3),3); 
    
    Wr = sum(bsxfun(@times,W1r,X1),3) + sum(bsxfun(@times,W2r,X2),3) + ...
        sum(bsxfun(@times,W3r,X3),3);
    
    Wt = sum(bsxfun(@times,W1t,X1),3) + sum(bsxfun(@times,W2t,X2),3) + ...
        sum(bsxfun(@times,W3t,X3),3);
    
    Sr = sum(bsxfun(@times,S1r,X1),3) + sum(bsxfun(@times,S2r,X2),3) + ...
        sum(bsxfun(@times,S3r,X3),3); 
    
    St = sum(bsxfun(@times,S1t,X1),3) + sum(bsxfun(@times,S2t,X2),3) + ...
        sum(bsxfun(@times,S3t,X3),3); 
    
    Srt = sum(bsxfun(@times,S1rt,X1),3) + sum(bsxfun(@times,S2rt,X2),3) + ...
        sum(bsxfun(@times,S3rt,X3),3); 
    
    Pi = sum(bsxfun(@times,Pi1,X1),3) + sum(bsxfun(@times,Pi2,X2),3) + ...
        sum(bsxfun(@times,Pi3,X3),3);
    
    % Clearing the Xi variables for reuse in the next element
    clear('X1','X2','X3','n','N');
    
    % Transforming the displacement field from the polar to the Cartesian
    % referential.
    % All pages in T are equal, so the first one is selected to compute the
    % normal cosines.
    Ux = cos(T(:,:,1)).*(Ur) - sin(T(:,:,1)).*(Ut);
    Uy = sin(T(:,:,1)).*(Ur) + cos(T(:,:,1)).*(Ut);
    Wx = cos(T(:,:,1)).*(Wr) - sin(T(:,:,1)).*(Wt);
    Wy = sin(T(:,:,1)).*(Wr) + cos(T(:,:,1)).*(Wt);
    
    % Transforming the stress field from the polar to the Cartesian
    % referential.
    Sx = cos(T(:,:,1)).^2.*Sr + sin(T(:,:,1)).^2.*St - ...
        2.*sin(T(:,:,1)).*cos(T(:,:,1)).*Srt;
    Sy = cos(T(:,:,1)).^2.*St + sin(T(:,:,1)).^2.*Sr + ...
        2.*sin(T(:,:,1)).*cos(T(:,:,1)).*Srt;
    Sxy = sin(T(:,:,1)).*cos(T(:,:,1)).*(Sr-St) + ...
        (cos(T(:,:,1)).^2 - sin(T(:,:,1)).^2).*Srt;
    
    % Updating the Loops structure by adding the new spectral results,
    % scaled back with the scaling coefficients
    LocResults.Ux{ii} = Ux * Scales.U;
    LocResults.Uy{ii} = Uy * Scales.U;
    LocResults.Wx{ii} = Wx * Scales.U;
    LocResults.Wy{ii} = Wy * Scales.U;
    LocResults.Sx{ii} = Sx * Scales.F;
    LocResults.Sy{ii} = Sy * Scales.F;
    LocResults.Sxy{ii} = Sxy * Scales.F;
    LocResults.Pi{ii} = Pi * Scales.F;
    
end
end