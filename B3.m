function [Bline, Bcol, Bval, CrtLineB] = B3(Edges, Loops, Bline, Bcol, Bval, CrtLineB, abscissa, weight)
% B3 sweeps through the edges and calls the functions that generate the
% B3 block of the boundary matrix in the LHS.
%
% B3 is called by MAIN***. It receives as input data the Edges and Loops
% structures, the LHS matrix (that is, the matrix of coefficients of the
% solving system), and the Gauss-Legendre integration parameters abscissa
% and weights. It returns to MAIN*** the LHS matrix with the B3 blocks of
% all elements inserted at the correct positions (as determined in
% ASSIGNPARTS).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Moldovan ID - Hybrid-Trefftz Finite Elements for Elastodynamic
% Analysis of Saturated Porous Media, PhD Thesis, Tehnical University of
% Lisbon
% 3. FreeHyTE Biphasic Transient User's Manual - 
%    https://drive.google.com/open?id=1GOCmCGzujusjkC2HGqjfKllDl55WO1HH
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developer’s 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% B3 computes the internal product between the following bases, expressed
% in a polar (r,th) referential:
% * the displacement basis U3, 
%   U3 = | Ur | = 1/2 * | i*beta_s^-1*(J(n+1,z_s)+J(n-1,z_s))         | * exp(i*n*th)
%        | Ut |         | beta_s^-1*(J(n+1,z_s)-J(n-1,z_s))           |
%        | Wr |         | i*gamma_s*beta_s^-1*(J(n+1,z_s)+J(n-1,z_s)) |
%        | Wt |         | gamma_s*beta_s^-1*(J(n+1,z_s)-J(n-1,z_s))   |
% * the boundary traction basis Z, defined by Chebyshev polynomials,
%        Z  = cos(m*ArcCos(abscissa))
% where n and m are the line and column of the current term in matrix B3.
%
% Consistent with the direction of the boundary traction approximation, the 
% displacement basis is projected onto the boundary normal or tangential
% direction,
%        Un = nr*Ur + nt*Ut
%        Ut = -nt*Ur + nr*Ut
%        Wn = nr*Wr + nt*Wt
% where nr and nt are the radial and tangential components of the outward 
% unit normal to the current boundary.
%
% Further details on the procedure are presented in reference [2]. 

%% Sweeping the edges
for ii=1:length(Edges.type)
    
    % Boundary blocks are constructed for Dirichlet, Absorbing, Robin and interior
    % boundaries only.
    if (strcmpi(Edges.type(ii),'D') || strcmpi(Edges.type(ii),'A') ||...
             strcmpi(Edges.type(ii),'R'))
        % LocEdge is a local structure where the features of the current
        % edge which are directly useful for the calculation of the
        % boundary block are stored.
        LocEdge =struct('nini',Edges.nini(ii),'nfin',Edges.nfin(ii),...
            'parametric',Edges.parametric(ii,:), 'lleft',Edges.lleft(ii),...
            'lright',Edges.lright(ii), 'order',Edges.order(ii),...
            'insert',Edges.insert(ii,:), 'dim',Edges.dim(ii,:));
        
        % Generating the boundary block corresponding to the current edge
        % and its LEFT finite element
        if LocEdge.lleft
            id = LocEdge.lleft;
            sign = 1.;
            % LocLoop is a local structure where the features of the left
            % element which are directly useful for the calculation of the
            % boundary block are stored.
            LocLoop = struct('id',id,'edges',Loops.edges(id,:),...
                'center',Loops.center(id,:),'order',Loops.order(id),...
                'insert',Loops.insert(id,3),'dim',Loops.dim(id,3),...
                'vibration',Loops.vibration(id,:));
            
            % The boundary blocks are generated separately in the normal
            % and tangential directions, if approximations are present. 
            % jj = 1 corresponds to the normal direction in the solid phase 
            % jj = 2 denotes the tangential direction in the solid phase
            % jj = 3 is the normal direction in the fluid phase
            for jj = 1:3 
                % Verifying if an approximation is present in the jj
                % direction. If no approximations are present,
                % LocEdge.dim(jj) is null (see ASSIGNPARTS).
                if LocEdge.dim(jj)    
                    % Launching the function that computes the boundary
                    % block for the current edge and its left element, in 
                    % the jj direction.
                    B3i = sign*B3_Matrix_i(LocEdge, LocLoop, jj, abscissa, weight);
                     
                    % getting the sparse vectors from B3i
                    [LB, CB, VB] = find(B3i);
                    
                    % Inserting the matrix in the B sparse vectors.
                    Bline(CrtLineB:CrtLineB+length(LB)-1) = LocLoop.insert+LB-1;
                    Bcol(CrtLineB:CrtLineB+length(CB)-1) = LocEdge.insert(jj)+CB-1;
                    Bval(CrtLineB:CrtLineB+length(VB)-1) = -VB;
                    
                    CrtLineB = CrtLineB + length(LB);
                    
                end
            end 
        else                       % there should always be a left element
            error('local:consistencyChk',...
                'No left loop for edge %d. \n', ii);
        end

        % Generating the boundary block corresponding to the current edge
        % and its RIGHT finite element        
        if LocEdge.lright
            id = LocEdge.lright;
            sign = 1.;
            % LocLoop is a local structure where the features of the right
            % element which are directly useful for the calculation of the
            % boundary block are stored.
            LocLoop = struct('id',id,'edges',Loops.edges(id,:),... 
                'center',Loops.center(id,:),'order',Loops.order(id),...
                'insert',Loops.insert(id,3),'dim',Loops.dim(id,3),...
                'vibration',Loops.vibration(id,:));
            % The boundary blocks are generated separately in the normal
            % and tangential directions, if approximations are present. 
            % jj = 1 corresponds to the normal direction in the solid phase 
            % jj = 2 denotes the tangential direction in the solid phase
            % jj = 3 is the normal direction in the fluid phase
            for jj = 1:3 
                % Verifying if an approximation is present in the jj
                % direction. If no approximations are present,
                % LocEdge.dim(jj) is null (see ASSIGNPARTS).
                if LocEdge.dim(jj)
                    % Launching the function that computes the boundary
                    % block for the current edge and its right element, in 
                    % the jj direction.
                    B3i = sign*B3_Matrix_i(LocEdge, LocLoop, jj, abscissa, weight);
                     
                    % getting the sparse vectors from B3i
                    [LB, CB, VB] = find(B3i);
                    
                    % Inserting the matrix in the B sparse vectors.
                    Bline(CrtLineB:CrtLineB+length(LB)-1) = LocLoop.insert+LB-1;
                    Bcol(CrtLineB:CrtLineB+length(CB)-1) = LocEdge.insert(jj)+CB-1;
                    Bval(CrtLineB:CrtLineB+length(VB)-1) = -VB;
                    
                    CrtLineB = CrtLineB + length(LB);
                 
                end
            end    
        end
    end
end
end

function B3i = B3_Matrix_i(LocEdge, LocLoop, jj, abscissa, weight)
% B3_MATRIX_I local function computes the B3 boundary block for the LocEdge
% edge and LocLoop element, in the jj direction. The side is mapped to a 
% [-1,1] interval to perform the integration.

%% Initialization 
% Initialization of the B3 block
B3i = zeros(LocLoop.dim,LocEdge.dim(jj));

% Loading vibration properties
gamma = LocLoop.vibration(3);
beta = LocLoop.vibration(6);

% n + LocLoop.order + 1 is the current line; m+1 is the current column
n = -LocLoop.order:LocLoop.order;
m = 0:LocEdge.order;

%% Generating the geometric data
% The following code transforms the abscissa coordinates, expressed in
% the [-1,1] referential, to the polar coordinates required to compute
% the values of the basis functions. The components of the outward
% normal to the boundary in the radial and tangential directions are
% also calculated. They are required to compute the normal and
% tangential components of the displacement basis.

% Computing the length of the current edge
L = sqrt(LocEdge.parametric(3)^2 + LocEdge.parametric(4)^2); % length

% Constructing the 3D matrices containing the n x m x abscissa
% integration grid
[N,M,A] = ndgrid(n,m,abscissa);

% Transforming the edge abscissa into local coordinates. The local
% referential is centered in the barycenter of the element, its axes
% aligned with the Cartesian axes of the global referential.
loc_x = LocEdge.parametric(1) - LocLoop.center(1) + 0.5 *...
    (A + 1) * LocEdge.parametric(3);  
loc_y = LocEdge.parametric(2) - LocLoop.center(2) + 0.5 *...
    (A + 1) * LocEdge.parametric(4);

% Transforming the local Cartesian coordinates into polar.
R = sqrt(loc_x.^2 + loc_y.^2);
T = atan2(loc_y, loc_x);

% Computing the components of the outward normal in the Cartesian
% directions.
nx = LocEdge.parametric(4) / L;   
ny = -1* LocEdge.parametric(3) / L;
if LocEdge.lright==LocLoop.id  % if the element is on the right,
    nx = -nx;                  % change the sign of the normal
    ny = -ny;
end

% Computing the components of the outward normal in the polar directions.
NR = nx * cos(T) + ny * sin(T);   
NT = -1*nx * sin(T) + ny * cos(T);

%% Computing the basis functions for all integration points
% Polar components of the displacement basis
Ur = 1i*beta^-1 * (besselj(N+1,beta*R) + besselj(N-1,beta*R))...
    .* exp(1i*N.*T);
Ut = beta^-1 * (besselj(N+1,beta*R) - besselj(N-1,beta*R))...
    .* exp(1i*N.*T);
Wr = 1i*gamma*beta^-1 * (besselj(N+1,beta*R) + besselj(N-1,beta*R))...
    .* exp(1i*N.*T);
Wt = gamma*beta^-1*(besselj(N+1,beta*R) - besselj(N-1,beta*R))...
    .* exp(1i*N.*T);

% Computing the normal or tangential projection of the displacement basis,
% according to the current direction, jj
if jj == 1  % normal direction
    U = NR.*Ur + NT.*Ut;
elseif jj == 2   % tangential direction
    U = -NT.*Ur + NR.*Ut;
else  % normal direction, fluid
    U = NR.*Wr + NT.*Wt;
end

% Computing the traction basis - Chebyshev polynomials mapped on the
% abscissa [-1,1] interval are used.
Z = cos(bsxfun(@times,M,acos(A)));

%% Computing the boundary integral
% The integral is the internal product between the displacement basis U
% (projected in the direction jj) and the traction basis Z
B3i3D = conj(U).*Z;
w3D(1,1,:) = weight;
% '1/2' is the shape function multiplier, not included in the expressions
% above
B3i = 1/2* L/2 * sum(bsxfun(@times,B3i3D,w3D),3); % computes the integral

end